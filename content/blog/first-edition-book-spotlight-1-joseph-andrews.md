+++
author = "Sam"
categories = ["design", "layout", "joseph andrews", "henry fielding", "literature", "hobbyist gaming", "table games", "tabletop games", "board games"]
date = 2021-08-06T11:00:00Z
description = "If you read the Iliad, you can laugh at funny stories, but if you haven't, Sam can explain why they are funny anyway."
lead-image = ""
tags = ["henry fielding", "joseph andrews", "picaresque", "comedy", "layout", "art", "texts", "cards", "literary history", "literature", "aesthetics", "design"]
title = "*First Edition* Book Spotlight 1: *Joseph Andrews*"

+++
Hi all! Over the next few weeks, as we continue to settle on illustrations for the 135 book cards that will be featured in _First Edition_, I am going to be writing a series featuring books and art that I particularly like.

So far, the art we have found belongs to one of two categories: art from original or early editions of the books, on the one hand, and art that I feel particularly hits upon the themes and spirit of the book it will be representing. Both kinds of art have been fun to find!

For Henry Fielding’s [_Joseph Andrews_](https://en.wikipedia.org/wiki/Joseph_Andrews), we’ve been lucky enough to find not one but _three_ pieces of original art from the novel. _Joseph Andrews_ is one of the first major novels written in English, published in 1742. It is a [picaresque novel](https://en.wikipedia.org/wiki/Picaresque_novel), which means that we largely follow the protagonists (the titular Joseph and his friend Parson Adams) as they travel around the countryside, getting themselves into all kinds of trouble. Some other well-known picaresque novels include [_Don Quixote_](https://en.wikipedia.org/wiki/Don_Quixote) and [_The Adventures of Huckleberry Finn_](https://en.wikipedia.org/wiki/Adventures_of_Huckleberry_Finn).

The most famous picaresque novels are generally comedic: the form gives the author a chance to survey a large variety of typical goings-on in whichever society he or she may be depicting. And yet the form is also usually used to make rather serious commentaries on that society and its ills. _Joseph Andrews_ is no different, and behind Fielding’s comic portrayals of such things as Adams being dunked in a cauldron of water in the kitchen or being chased by a pack of hounds who have mistaken him for a deer is a serious, moralistic message.

That said, the comedic surface of Fielding’s novels are what I like most about them, and that surface is definitely what’s reflected in the image we will likely be choosing out of the three available to us. Because the episode of Adams being hunted by hounds is the one that I remember most vividly when I think of _Joseph Andrews_, I’m leaning toward using this image:

![](/uploads/joseph_andrews_1.jpg)

The image is funny, especially if you’ve read the book, but it only captures a small portion of the comedy of the scene. I say this because Fielding writes the scene as if it’s part of [_The Iliad_](https://en.wikipedia.org/wiki/Iliad), with heroic, formal language, including an epic list of the names of the hounds. The contrast between Fielding’s language and the event that’s actually occurring is what makes it so funny, and having this image to look at as you read the scene certainly drives that point home!

I hope you enjoyed this little overview of _Joseph Andrews_ and the art we’ll be using for it. Stay tuned for another selection next week!

\--Sam