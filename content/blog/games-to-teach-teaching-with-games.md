+++
author = "Tom"
categories = ["design", "pedagogy", "practice"]
date = 2021-04-13T11:00:00Z
description = "Are there lesson plans for board games?"
lead-image = ""
tags = ["resources", "education", "learning", "teaching"]
title = "Games to Teach, Teaching with Games"

+++
How do you use games in your classroom?

I've been thinking a lot about what it looks like to use tabletop games in actual classrooms, with actual students, especially as we all try to get back to a More Normal World where humans spend time in rooms together doing things with physical objects and reacting to faces that aren't on screens. Specifically, I've been poking around the web in search of examples of **available lesson plans** that use games to teach individual concepts to students at different grade levels, whether as a classroom-wide event or in smaller groups.

"Gamification" has been something of a buzzword in corporate spaces for a while now, [for better](https://www.nytimes.com/2012/12/24/technology/all-the-worlds-a-game-and-business-is-a-player.html) or [for worse](https://www.theatlantic.com/technology/archive/2011/08/gamification-is-bullshit/243338/). As far as I can tell, in tech -- and I say this as an external viewer -- it's generally used to describe a set of techniques for making products (especially apps) [more engrossing](https://yukaichou.com/gamification-examples/what-is-gamification/#.Vbja4-64Ng0). In our glorious capitalist world, the point of making things is to make more money, and if you make things people like to spend more time with, they will spend more money on those things; and, _voilà_, profit! You can make "gamification" "actionable" so as to "transform your user-base" and "disrupt your industry" etc etc etc.

I initially thought, as someone that is interested in learning and also in games, that I would be interested in "gamification." Now, I'm starting to suspect that I'm interested in whatever the inverse of gamification is. That's not to say I'm against making inhumane tasks more human! I'm not against profit, really, or innovation, or marketing, or any of these things, at least not categorically.

But when I think of the tabletop games I have learned from -- and the ones I love to teach my family and friends -- I don't think about "actionable transformations" of how they understand [limited market dynamics](https://boardgamegeek.com/boardgame/11/bohnanza), [cost-value analysis](https://boardgamegeek.com/boardgame/148228/splendor), or [North American birds](https://boardgamegeek.com/boardgame/266192/wingspan). I think, first, of how their mechanics and their aesthetics caused me to think differently. In my experience, "gamification" in the corporate sense has little to do with the experience of learning about the world and its wonders.

That said, I think games like _Bohnanza_, _Splendor_, and _Wingspan_ could be "used" to great effect to teach otherwise difficult concepts. I played _Machi Koro_ for the first time last week (and rolled 3 after 3 after 3, a rant for another time), and it struck me as an amazingly transparent tool for thinking about expected value. You don't even have to know what expected value _is_ in order to understand the basic risks and rewards involved in buying a card that gives you 1 coin anytime anyone rolls a 2 versus a card that gives you 3 coins, but only when you yourself roll a 4!

So I can imagine using [_Machi Koro_](https://boardgamegeek.com/boardgame/143884/machi-koro) to great effect in a middle school math classroom, to develop math sense, or in a college-level introductory course on probability as a testing ground for theory. And [other people](https://moontowermeta.com/let-your-kids-play-boardgames/) [have had](https://www.teachers-tools.com/games-toys/games/machi-koro-game.html) [similar thoughts](https://boardgamegeek.com/thread/1861008/quantifying-benefits-board-games)!

But my sense is that, while [a lot of people](https://boardgamegeek.com/forum/35/bgg/games-classroom) will agree that _games are good teaching tools_, not a lot of nuts-and-bolts lesson plans are widely available for _teaching specific games with specific learning objectives in mind_.

What content am I missing out there? Where should I start? What are some lesson plans you've developed? For which games?

\-- Tom