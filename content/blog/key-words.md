+++
author = "Sam"
categories = ["mindset", "explanation", "update"]
date = 2021-04-30T12:00:00Z
description = "Words and pictures are the key to success"
lead-image = ""
tags = ["copyright", "problem-solving", "expertise", "aesthetics", "art", "design"]
title = "Key Words"

+++
Work continues apace at Tower Grove Games on _First Edition_. Right now, we are feeling our way through our messiest question so far: how does a newly-formed board game company, looking to fund our first Kickstarter, go about creating high-quality art for a game that contains over 200 cards? On the one hand, we want the art to be a selling-point and we want the game to have a quality of art that it deserves so that it can really shine. On the other hand, we are not artists, nor do we have that much, um, disposable income!

Now, you may be asking: _First Edition_ is about old books. Why not just use the first edition covers of those books? Surely they are out of copyright at this point. Well, the first problem with that involves some light book history: most of the books in _First Edition_ just…didn’t have covers. That’s right: people would buy a book, on paper, and then they would _bind it themselves_ with, like, leather and thread. So books in the 18th century didn’t have cover art. They might have had illustrations—even a front-page illustration called a "[frontispiece](https://www.merriam-webster.com/dictionary/frontispiece)"—but not covers.

OK, well why not just use the frontispieces then? On that, I’ll refer you to Tom’s ongoing series of posts, but I will also just say that if someone in the twenty-first century takes a digital image of something made in the 18th, then that image is copyrighted even if the 18th-century thing is not. And, unfortunately, neither Tom nor I have a first-edition _Robinson Crusoe_—a book written before copyright even existed—lying around!

So, as we explore ways of securing public domain images that are evocative of the books we are representing in _First Edition_, we’ve decided it makes sense for me—the person who listed out all of these books in a spreadsheet three years ago, after all—to come up with key words for us to search in the various databases of images we’ve uncovered thus far.

There are 135 book cards in _First Edition_, of which I’ve read 73. It’s already difficult to come up with key words for some of the books I _have_ read—how do you come up with _different_ key words for _Pride and Prejudice_ and _Emma_, for instance?—but even harder for those I already only have a passing familiarity with—Sir Walter Scott’s novels _Waverly_, _Rob Roy_, and _The Bride of Lammermoor_ currently all just say “Scotland.” While this obviously poses a problem, it’s also a fun one—I get to learn more about a set of novels I’ve heard of but haven’t fully experienced. It’s like finally working through your Board Game Shelf of Shame, but for someone with (almost) a PhD in English Literature!

--Sam