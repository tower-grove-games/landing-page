+++
author = "Tom"
categories = ["surprise", "praise", "design", "knowledge", "learning", "tabletop games", "board games"]
date = 2021-06-15T11:00:00Z
description = "people on the internet are nice, sometimes"
lead-image = ""
tags = ["learning", "knowledge", "composition", "mechanics", "theme", "feedback", "advice", "brainstorming", "design"]
title = "Knowing and Learning on the Facebook Design Lab"

+++
I confess, I do not much like the "face book." Like most social media, it's only getting worse; and sometimes I wonder if it was ever really better, or if I just was paying less attention a decade ago than I do now. (Probably both.) I am a naturally enthusiastic person and a wary social media user, and my inclinations are toward lurking first and exclaiming (in the good way) later. Still, I understand forming connections online is Part of the Job if you want to make games that people buy (which we 100% do), so this week, I took a deep breath, wrote a rather chipper note, and put it out there for the Board Game Design Lab page to see.

IT WAS AMAZING! If you are a person who responded, and you are reading this, from the bottom of my tentative little heart -- thanks a million. I can't wait to see the same [community on BGDL+](https://bgdlplus.com/).

See, I have been [tinkering with this game idea about medieval manuscripts](https://towergrovegames.com/blog/just-blank-paper/), which I'm calling _Codicologica_. As I mentioned last week, I've settled on some broad mechanics that make the thing tick, though I'm sure those will need to be tweaked as time goes on. There will be some light economic components, but mostly it's an abstract game: you're gaining unique cards with a set of specific features and using those cards to get points. The catch is that the cards are _texts_ that you are copying into your _book_. So you don't really have to know what the cards _mean_, to play the game -- but the game will probably be more fun if at least some of those texts are _recognizable_.

I have been hesitant to start naming individual texts for those cards, because I want to find a balance between historical accuracy and everyday familiarity. Technically, they could be anything: unique IDs, made-up titles, astronomical entities, whatever. But I want to use real medieval texts and real medieval books, as much as I possibly can, without lying. I do have some insider knowledge, after all.

The big problem is that so much of what we think of as "medieval" is actually a modernized version of the medieval past, and so much of what is actually "medieval" is totally unrecognizable to intelligent, literate, interested non-specialists. [(I teach about this!)](https://tcsawyer.github.io/teaching.html) Most people with a Ph.D. in medieval studies are unfamiliar with the things I specialize in! (And, to be fair, am I unfamiliar with their specializations: that's what a Ph.D. does to your brain).

If I started by listing [the most popular texts](https://www.google.com/books/edition/The_Latin_Poems_Commonly_Attributed_to_W/ZTclGutUVmEC?hl=en&gbpv=1&dq=thomas+wright+walter+mapes&printsec=frontcover) in, say, the actual 14th century, the vast majority of _English PhDs in medieval studies_ would never have heard of them, much less the vast majority of humans who I think would find this game enjoyable. But if I started by listing the so-called "medieval" texts that the vast majority of humans would recognize -- the list would either be very small or frustratingly (for me!) incorrect.

In any case, the warm, welcome, truly encouraging advice and feedback I received from the BGDL facebook group eased many of my fears, and gave me confidence that I can include a smattering of recognizable texts AND a smattering of specialist knowledge in the same game. The trick will be finding balance, and it is really heartwarming to know that gamers embrace balance too, between what we already know and what we can learn.

\-- Tom