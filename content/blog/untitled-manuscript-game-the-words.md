+++
author = "Tom"
categories = ["playtesting", "play", "design", "gaming", "games", "hobbyist games", "board games", "tabletop games"]
date = 2021-07-27T12:00:00Z
description = "What words do you put in games about putting words in books!?"
lead-image = ""
tags = ["feedback", "theme", "composition", "knowledge", "passions", "alpha", "theory", "components", "brainstorming", "design"]
title = "*Untitled Manuscript Game*: The Words"

+++
So the conceit of _Untitled Manuscript Game_ is that you are a medieval scribe, and you are making yourself a manuscript full of delicious texts. I have a bunch of notes on the mechanics, and with any luck next week's post is a solid overview of the rules in Semi-Official Rules Form; but basically you'll spend the game acquiring texts, writing them into your book, and scoring points for how long or dense or alike they are in some fashion or another.

But the big picture stuff is out of the way: [I brainstormed that with Lauren while camping](https://towergrovegames.com/blog/just-blank-paper/), then Sam and I did a few poolside theoretical modifications the following weekend. (It turns out, no one else likes the name, _Codicologica_. FINE.)

![A very cool page from a very cool book.](https://upload.wikimedia.org/wikipedia/commons/thumb/b/bf/Auchinleck_Manuscript_-_Reinbroun_%285372467381%29.jpg/640px-Auchinleck_Manuscript_-_Reinbroun_%285372467381%29.jpg "Wikimedia Auchinleck MS")

Scholar (read: nerd) that I am, I decided to base the game on actual manuscripts and their actual texts, including a few that I have studied in depth (yowza) and a few that are, as I would put it in my professor-voice, "critical repositories of a vast medieval European literary tradition" -- or something like that. Funny enough, this aligns with some of my research, as I am increasingly interested in examining how textual overlap between individual manuscript copies might change the way we read the texts in question.

![Dang, y'all, this book is HUGE. 80 lines to a column like yikes.](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Vernon_Manuscript_Folio_265r.png/640px-Vernon_Manuscript_Folio_265r.png "Wikimedia Vernon MS")

This is a tricky task! I wrote about some of the [problems with picking crossover texts](https://towergrovegames.com/blog/knowing-and-learning-on-the-facebook-design-lab/) for a general audience in June.

![Names of old books and their old texts 1](/uploads/umg_texts_0-0.jpg "UMG MSS Set 1")

In any case, this past week I finally nailed down my list of manuscripts and their relevant texts for the game (since I can't hardly include every text that's in each manuscript I choose!). This means I also finally have my first set of working individual texts (since some are repeated between the game's model manuscripts)!

![Names of old books and their old texts 2](/uploads/umg_texts_0-2.jpg "UMG MSS set 2")

My goal for the upcoming week is to take raw information about these cards and translate that information as best I can into usable cards for play. (Don't worry: you don't have to copy all 25,000 lines of the _South English Legendary_ just to play the game!) I also need to create a quick playtesting grid so we can get this thing on the table ASAP -- which shouldn't be too tough, I don't think?

![Names of old books and their old texts 3](/uploads/umg_texts_0-3.jpg "UMG MSS Set 3")

See any texts there that really appeal to you? Anything you're excited to copy into your untitled manuscript play?

\-- Tom