+++
author = "Sam"
categories = ["literature", "plot", "art", "james joyce", "ulysses", "first edition", "play", "design", "hobbyist games", "table games", "tabletop games", "board gaming", "board games"]
date = 2021-09-03T12:00:00Z
description = "If you read Ulysses and like it -- like, actually like it, not just pretend to like it because you think you should -- read Sam's post. And consider grad school?"
lead-image = ""
tags = ["literature", "ulysses", "james joyce", "art", "first edition", "texts", "cards", "components", "aesthetics", "design"]
title = "*First Edition* Book Spotlight 4: *Ulysses*"

+++
Happy Friday everybody!

My book spotlight this week is on a novel that’s near and dear to me, James Joyce’s [_Ulysses_](https://en.wikipedia.org/wiki/Ulysses_(novel)). _Ulysses_ is famously difficult, for a few reasons. First, it’s just long: nearly 700 pages, and the pages themselves are big. But the length is just the beginning. The real difficulty comes from the fact that _Ulysses_ is an intensely experimental novel; each of the 18 chapters features a different, extended experiment with literary style. So, Chapter 7, “Aeolus,” recounts Leopold Bloom’s visit to his place of work, a newspaper office, in the style of a newspaper. Each small plot point is announced as a newspaper headline, followed by a short newspaper-style account of what Bloom and his compatriots are doing. Chapter 17, “Ithaca,” by contrast, is written in the style of a Catechism, and features 309 questions and answers that recount Bloom’s return home at the end of the day.

The stylistic shifts between chapters, combined with the frankly ridiculous breadth and depth of allusions to outside events, people, places, and historical moments makes _Ulysses_ a very difficult read. But the flip side of that token is that when I encountered it for the first time as an undergraduate English major, the fact that I was able to unravel most of what was going on (with the help of a guide book!) made me feel smart. And while _Ulysses_ alone didn’t convince me to go to grad school, I do think it had an outsized influence on my choice of subfield (British modernism) once I got there.

As this description starts to establish, the draw of _Ulysses_ is not the story but the writing itself. That poses quite a challenge when trying to find a representative picture! That said, I’ve always thought that the most remarkable part of the content (as opposed to the form) of the novel is the intense detail with which Joyce renders a single day in Dublin, Ireland, in the summer of 1904. In one chapter, for instance, Joyce represents Bloom walking through the city. At the same time, he has several other minor characters set out from various points in the city, heading in different directions. As the chapter goes along, these various figures cross each others’ paths at the precise moment and in the precise place they would have given the configuration of Dublin’s streets on that day—roughly 18 years before Joyce wrote the novel. The level of attention to detail and of precision in Joyce’s rendering of Dublin shines through all of his stylistic innovations, and so I wanted to choose an image that features the city itself.

This is where I landed, on a picture of a famous Dublin street in 1904, roughly when _Ulysses_ is set:

![](/uploads/joyce_1.jpg)

I like this image a lot: I think it conveys the impression of a bustling city (which is certainly true to Joyce’s novel), featuring people crossing the street and street cars, while still focusing on the two statues that tower above it all (and that are featured in _Ulysses_). I also like the advertisement for Tylers’ Boots on the streetcar, since _Ulysses_ is filled with references to advertisements that Bloom or another of the characters sees. Finally, the perspective of the photograph—above Dublin, looking out into its heart—perfectly captures Joyce’s, and implies that the entire city is the novel’s (and the novelist’s) purview.

Let me know what you think!

\--Sam