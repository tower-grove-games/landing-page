+++
author = "Tom"
categories = ["not a lawyer", "academia", "description", "reflection"]
date = 2021-05-04T12:00:00Z
description = "Don't steal from megacorporations in academia."
lead-image = ""
tags = ["legal", "creative", "strategy", "images", "copyright", "design"]
title = "Copyright, Cover Art, and the Idea of Property (II)"

+++
### MAY THE FOURTH BE WITH YOU!

. . . .

So, [last week](https://towergrovegames.com/blog/copyright-cover-art-and-the-idea-of-property-i/) I was writing about the names of things and whether they could be copyrighted. We learned that, [if we tweet](https://twitter.com/TowerGroveGames/status/1387060470805274626) at Fantasy Flight Games, asking them not to sue us, they won't! Or at least, they haven't so far. Thanks, FFG! Thanks to anyone not suing us or anyone else!

In the meantime, Sam has given some [better context](https://towergrovegames.com/blog/key-words/) on why finding historically appropriate images might pose a particular problem for early print books in _First Edition_. We learned many books were printed and sold without any bindings at all; and I'll add the bonus fact that even more of the books on Sam's list (like: the majority of novels by Charles Dickens) were printed first as serials in magazines, not as collected story units. (I am a book historian, sure; but I learned that from [_The Perks of Being a Wallflower_](https://www.imdb.com/title/tt1659337/).)

This week, I'm going to write a bit more about images -- what they are of, who stores them, in what forms, and according to what rules. Once again, I'm not a legal expert. I'm just a guy on the internet trying to figure out how to use cool pictures in fun games.

. . . .

So, there's no problem printing a card with the words "_Robinson Crusoe_ by Daniel Defoe" on a piece of paper and then selling that piece of paper. In fact, there's no problem with printing the entirety of the text that _Robinson Crusoe_ wrote and selling it, because Defoe has been dead for a long time, and copyright was then in its most nascent stages of being. (NB: this would not be a lucrative business venture.)

What about printing a card with an image from some early version of _Rob Cru_? I have the resources of a modern research university at my fingertips, which means I can access _ECCO_ ([Eighteenth Century Collections Online](https://www.gale.com/intl/primary-sources/eighteenth-century-collections-online)), and I can find the earliest printed frontispiece for _Rob Cru_ (in its third print run, but still the first edition), and I can copy that image, and I can save it to my hard drive, and I can pore over its details, talk about it with my friends, and generally Have Thoughts about it and Share Thoughts with the world freely (as long as no one pays me for those thoughts).

If I'm writing a paper on the history of _Rob Cru_ for academic presentation, this is not a problem. If I'm submitting that paper to a journal that nerdy humans pay money for, I need to get permission from . . . someone? I think [_GALE: A Cengage Company_](https://www.gale.com/int)? ("Proud to be one of the world's seven digital academic overlords.") But odds are they say, "yeah, sure, that's scholarship, which is educational use, and you've done the courteous thing, and here, have at, go crazy."

If I am making a tabletop game (or, if Sam is, and I'm pretending to help) -- even if it's a game made explicitly with educational aims in mind! even if it comes with a teaching guide! -- then I still need to get permission from . . . whoever . . . to use the image that I downloaded from _ECCO_, hosted by _GALE_. And odds are, they will say something like, "lol what, no, you gotta give us a couple hundred bucks to use that image, and if you don't ask nicely and pay us the money we will send our lawyerfolks to take all $37.80 in your LLC banking account and make your life terrible, because we are a major institution with resources and you are a dumb nerd who can't tell the difference between educational and commercial use."

That last thing sounds bad! It is bad! Also, we don't have a couple hundred bucks for each of 100+ distinct book cards in _First Edition_ because if we had that kind of money laying around for our incipient and as-yet-unfunded business ventures, we'd be on vacation forever.

My understanding is: _GALE_ can claim copyright on a representation of a woodcut imprint from 1719 because they have taken the time to find one of the four remaining copies of the 1719 third printing of _Rob Cru_, to snap a fancy photo, and to store that photo digitally on the internet, such that people with wacky institutional resources can access it and use it for normal academic reasons.

The copyright isn't on the actual picture in the actual book which was actually printed in 1719. ("That _would_ be crazy!" you say; you are wrong; tune in next week). The copyright is on the image stored at _ECCO_, because the terms of accessing that image come with restrictions and limitations on how that image can be used.

. . . .

Wow, I have a lot to talk about here, turns out I like copyright (or don't). I'm going to keep this series going next week, on the topic of what happens if I just go to the British Library and snap my own photo of _Rob Cru_, other places to snap photos, and other places to find photos. I'll pitch Corey Doctorow and see about making a couple of dutiful Marxist jokes. It will be fun. I'm not a lawyer.

\-- Tom