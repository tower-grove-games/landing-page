+++
author = "Tom"
categories = ["reflection", "walkthrough", "description"]
date = 2021-05-25T11:00:00Z
description = "You are reading the words of an entity"
lead-image = ""
tags = ["forms", "regulations", "rules", "code", "practicality", "business"]
title = "Being an LLC"

+++
Last week, I finally made my way down to City Hall and submitted the final documents necessary to operate as a multimember LLC in St. Louis City County, MO, USA. I filed with the State of Missouri all the way back in early April, which took approximately 10 minutes. The instructions online are very clear, and forms are easily accessible. I filed for a federal EIN on the same day, which took approximately 10 more minutes. The instructions online are also very clear, if _really long_, and forms are cross-linked obsessively (the hallmark of IRS-ness).

Then, I made the massive mistake of trying to sort out the business website for the City of St. Louis on my own. It took approximately 6 weeks, with the usual interruptions, including a final 3-hour stint wandering around City Hall at the end. Without the kind people of the Business Assistance Office -- a nonprofit that runs out of City Hall to help people like me precisely because the City's documentation is arcane, inaccessible, and actually not that complicated -- I might still be wandering. (Thanks Kelly!)

This post is a record for my memory, and for any lost souls that may have to travel the same path as me.

......

So I'm still not entirely clear on the order of operations here, but I believe two events could have happened concurrently.

First, since Tower Grove Games LLC is a design operation running mostly from my finished attic, I had to apply for a "Home Occupation Permit" from the division of building and zoning. I did this from the comfort of my home, in April. The only tricky thing was that, to fill out the form, I had to select one "industry" from a drop down list of thirteen options. Since I was not a "Restaurant" or "Construction" or anything of the sort, I found myself torn between "Writer" and "Consultant." But, look, I'm writing **right now**, though I'm not really writing for profit? Reader, I chose writer. Send money.

Second, because _Tower Grove Games LLC_ has founders and investors and whatnot, I had to run the whole operation by the Comptroller's Office. For this, St. Louis has devised the E-9 form. I believe they were verifying that none of us owed back-taxes? But maybe they were looking for other crimes, too! Anyway, the lady who helped me had an amazing array of MLK buttons on her wall, so I wasn't mad about being there.

Those tasks completed, approved by the Comptroller AND Zoning, I made my way to the Business Licensing Office. (Wow, very approve.) I had called this office approximately 4,000 times with questions before I went in, so it all felt very friendly. Huge shout out to Jerry, the clerk, the man, the legend. I'm sending that guy a free copy of our first game, so if you support _Tower Grove Games LLC_, you support Jerry. Think on that.

In the end, a nice lady looked over my paperwork, had me re-write or clarify a few things (e.g. I am not a sex offender!), and sent me to the cashier with a "Home Occupation License." That's different from the "Home Occupation _Permit_" I acquired previously!

I should note that the Business Licensing Office does not have a code for "Writer" -- or for "design," "development," or "tabletop entertainment." So you're reading the words of an "Other Business" entity! It's still unclear to me why any of these codes matter, and I suspect I will find out in the least enjoyable possible way when taxes come.

And now the company -- technically a "multimember LLC partnership"? -- exists! What a time to be alive.

\-- Tom