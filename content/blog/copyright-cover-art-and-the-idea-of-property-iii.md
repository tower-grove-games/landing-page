+++
author = "Tom"
categories = ["late capital", "resources", "reflection", "description", "still-not-a-lawyer"]
date = 2021-05-11T11:00:00Z
description = "Don't steal from the stealers."
lead-image = ""
tags = ["not-being-a-lawyer", "legal", "creative", "strategy", "images", "copyright", "design"]
title = "Copyright, Cover Art, and the Idea of Property (III)"

+++
This is the third part in an indefinitely long series of Tom discussing copyright in the capacity of not at all being a lawyer whatsoever at all. You can find [Part I here](https://towergrovegames.com/blog/copyright-cover-art-and-the-idea-of-property-i/) (on text and reference) and [Part II here](https://towergrovegames.com/blog/copyright-cover-art-and-the-idea-of-property-ii-hundreds-year-old-images-under-copyright/) (on digitally stored versions of very old pictures). Sam has also provided some helpful context on _First Edition_ [here](https://towergrovegames.com/blog/the-state-of-first-edition/) (a general overview) and [here](https://towergrovegames.com/blog/key-words/) (on tagging cards).

If you're a returning reader, you'll be happy to know that no one has sued us yet, which is pretty remarkable, I think, given that we exist in America, and also it's the twenty-first century!

To recap: I believe I can print the text of _Robinson Crusoe_ because I could find it nearly anywhere, and it's very out of copyright, and anyway it was never really under copyright in the first place, because the 1700s were a wild (read: awful) time, when literal human beings could be bought and sold and protected as property but creative works couldn't. Yikes, the "Enlightenment," what the hell.

But I also believe I can't reprint the frontispiece to the 1719 _Rob Cru_ because it is hard to find (there are, as far as I can tell, four total copies), and the digital version is within copyright, because a picture of a thing and a thing itself are not the same.

. . . .

So, dear reader, you might be wondering, "OK Tom, this all sounds dire, but what if you booked a flight to London, waltzed into the British Library, where they keep a very well-preserved copy of this picture you need a picture of, take your own picture, and waltz out?" (Let's leave aside the troubling facts that (a) I couldn't fly to London now if I wanted to because, hey there global pandemic, _que tal_; and also (b) I don't know how to waltz, I only do 4/4 time and that ska thing where you flail a lot but sort of rhythmically.) This is a great question!

The answer is: [I'd still have to get permission](https://forms.bl.uk/permissions/) -- this time from the British Library -- and license the use of the picture I took of the thing they stored. This would undoubtedly cost me [some units of currency](https://www.bl.uk/britishlibrary/\~/media/bl/global/services/digitisation%20services/permissions_fees_current.pdf?la=en&hash=CBE416D15843ADCF7C62359E00A9BF7C), and the number of units of currency it would cost me would undoubtedly reach impossible heights very quickly, given that Sam's very fun books game has over 100 cards for individual books in it. Also, I'd have to fly to London, a thing I do love doing, usually; but a thing that also costs units of currency.

"But why fly to London or use ECCO if the British Library already keeps digital versions of physical images on hand?" _Another_ great question, imaginary human who is definitely not a projection of myself! In fact the British Library has a [whole web page devoted to this question](https://www.bl.uk/on-demand/commercial#)! You can tweet at them (I almost certainly will) and call them using that sweet sweet +44 UK international region code! You can search literally any image in their collection and find out how much it would cost to print that image on a piece of paper and sell it to other humans for units of currency!

I should note that the British Library has partnered -- in my opinion, rather unhelpfully -- with two companies whose business it is to facilitate purchase and use of stored digital images of real physcial images that they did not themselves produce! You can check them out (I've chosen some sample images): [Superstock](https://www.superstock.com/stock-photos-images/1746-21117888) and [Bridgeman](https://www.bridgemanimages.us/en-US/asset/460026). 

Do you want an undated digital rendition of a picture from an unlisted production of _Rob Cru_ where Rob Cru looks kind of like if John the Baptist got extremely lost? (Why is he wearing furs _on the boat_?) Superstock has you covered! It's just a couple hundred dollars! 

Do you want a digital rendition of a picture from an 1897 production of _Rob Cru_ where Rob Cru looks kind of like Tom Sawyer if Tom Sawyer had a cat and also were better at rafts (which is more of a Huck thing, anyway)? Bridgeman has you covered! It's just a couple hundred dollars (image quality depending).

The list of very fancy museums and art collections using these facilitators is insane, given that neither the repository nor the facilitators created the vast majority of the original images (that they are selling digital renditions of) in question. I understand that it's hard to keep a _book_ or a _painting_ in tact for a couple hundred years, so that people like me can gawk at the pictures inside. But a picture of that book? A _digitized scan of a picture of that book_? I thought we lived in a society.

. . . .

I'm going to do exactly one more post about this, next week; mostly to collect some resources I've found helpful -- NOT BEING A LAWYER OR OFFERING LEGAL ADVICE -- in my pursuit of good-but/and-free images for excellent (or weird or whatever) old books. For now, toodle-oo, and thanks for reading.

\-- Tom