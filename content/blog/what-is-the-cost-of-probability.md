+++
author = "Tom"
categories = ["criticism", "praise", "reflection", "exposition", "design", "board games"]
date = 2021-06-22T11:00:00Z
description = "Time, mostly: that incalculable finitude that looms over all"
lead-image = ""
tags = ["duration", "mitigation", "actions", "balance", "expected value", "probability", "math", "design"]
title = "What is the Cost of Probability?"

+++
I've been making a point of commenting a bit more frequently on the BGDL facebook page, for reasons selfish and unselfish. The unselfish reason is that it's _good_ to talk to other humans about how games are designed, the structures of thought that go into them, the incentives they build in (or omit), the material problem of how pieces in your head become pieces on a table -- that kind of thing. The "why are you designing games" sort of thing. The "why do you like playing games in the first place" kind of thing.

The selfish reason is that facebook has algorithms, and those algorithms reward typing words into screen-boxes and hitting enter, and there will come a time when I want to type words into screen-boxes (and hit enter) in such a way that lots of people see them. (Those words will be, more or less, "please buy my game so that someday I can do this all the time and also feed my family, thank you." This, too, is a "why are you designing games" sort of thing, it turns out.)

Yesterday, a post caught my eye that opened: "what is the cost of probability?" I think, upon re-reading, that OP was actually asking a fairly straightforward question about [calculating expected value](https://en.wikipedia.org/wiki/Expected_value) for a game using cards to generate currency and dice to activate cards. That question was answered quickly! Good job, everyone.

\[I recommended most designers brush up on their general awareness of methods in probability; I come back to [the opentextbookstore](http://www.opentextbookstore.com/catalog.php?disc=Mathematics&subdisc=Statistics) a lot!\]

But the openness of the question as phrased spawned another series of answers that have less to do with equations and formulas -- and more to do with the very very tricky problem of accounting for opportunity cost in a closed system (i.e. a game) where the number of decisions players can make is finite.

Again, the instant-classic example for my house is [_Wingspan_](https://boardgamegeek.com/boardgame/266192/wingspan), where you take exactly 26 actions every game: if you want to win, you need each of those actions to generate around or just above 4pts on average. But those points don't come every turn, the accumulate in a snowballing mass over the course of your game. In my opinion, [_Parks_ handles](https://boardgamegeek.com/boardgame/266524/parks) this kind of limitation really well, without strictly specifying how many decisions you get overall. The setup determines how many opportunities you will get to activate each space, and your opponents refine your choices further. A counter-example [is _Terraforming Mars_](https://boardgamegeek.com/boardgame/167791/terraforming-mars): if everyone at the table is engine-building and no one is raising the temperature, it can last all. blasted. day. (_TM_ is my favorite game of chance.)

Anyway, as so many facebookers pointed out, in many games, cards don't only have a expected value, they also imply other costs: a card that says "if you roll a 5, get two points" may have an EV per roll of 2, but its terms will imply other valuations as well. How many times will you roll the dice in a single game? Do you have any control over that? Are there cards that yield fewer points more frequently? Are there cards that yield more points less frequently? How many cards are you going to get over the course of the game? Do you have any control over that?

The point is, if you can only get X cards per game and there will only be Y rolls, it puts the designer in a funny position. Numerically, a card that says "if you roll a 5, get ten points" \[action-EV \~ 1.7\] _should_ be worth significantly more over the course of a game than a card that says, "if you roll a 2 or a 3, get one point" \[action-EV \~ 0.3\]. If you roll the dice one million times to smooth out distribution, the first card probably wins by a landslide! But if you roll the dice 10 times, possibly you roll no 5's at all -- or possibly you roll only 5's!?! -- then one player will be grumpy and the other quite pleased, for reasons that have very little to do with strategy or the mitigation of chance through play. (This is my basic complaint about [Machi Koro](https://boardgamegeek.com/boardgame/143884/machi-koro), a game that pretends to allow you to mitigate chance while secretly incentivizing diversifying high-risk strategies.)

Usually, the way I have seen this solved is to bring the action-EV's closer together: either rolling a 5 yields fewer points or rolling a \[2 or 3\] rolls more. But that appears to be its own can of worms for next week, because maybe balance is boring? More on that topic, next week.

OH! And I made a [BGDL+](https://bgdlplus.com/) account (the free kind), hmu: tcsawyer.

\--Tom