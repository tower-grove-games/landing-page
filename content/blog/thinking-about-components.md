+++
author = "Sam"
categories = ["appreciation", "differentiation", "reflection", "analysis"]
date = 2021-05-15T12:00:00Z
description = "Sam's materialist critique of Spirit Island"
lead-image = ""
tags = ["education", "immersion", "theme", "components", "design"]
title = "Thinking About Components"

+++
I haven’t gotten to play [_Spirit Island_](https://boardgamegeek.com/boardgame/162886/spirit-island) in a long time because of the pandemic and my partner’s lukewarm feelings on the game. As I wrote [here](https://towergrovegames.com/blog/sam-s-top-5/), I love it for the richness of the puzzle it asks you to solve and for the complexity it presents in demanding you think at least three turns ahead. But another reason it stands out is its components.

_Spirit Island_ does not have the nicest components, nor is it the game I own that’s most “immersive” in terms of components—that title would have to go to [_Tainted Grail_](https://boardgamegeek.com/boardgame/264220/tainted-grail-fall-avalon), I think. Or maybe [_Scythe_](https://boardgamegeek.com/boardgame/169786/scythe) (especially since I’ve painted all of those minis!) What sets _Spirit Island_ apart is that the game’s components help tell the story that the game tells as a whole. _Spirit Island_ is an anti-colonial game in which players play as powerful spirits who band together to expel European colonists from their lands (aided by the indigenous people on the island, the fictional Dahan).

The components help tell this story not by attempting to mimic reality, but by abstracting from it: all of the components related to the spirits, Dahan, and nature are made of natural materials like wood and cardboard, while the colonists are represented by spindly, stark-white plastic miniatures. As they spread their plastic cities, towns, and patrols over the island, they also spread blight, again represented by a dull grey plastic piece.

In refusing realism, these components break a certain type of immersion. But they also emphasize the game’s central thematic concerns, pitting plastic against wood, and suggesting that to you, a spirit, these invading humans _only_ signify as faceless, plastic-covered threats to the natural world. It’s a powerful critique of an underacknowledged element of colonial expansion: the devastating effects it has on the environment. Importantly, this critique is not _substituted_ for an anti-colonial argument made on the basis of the violence done to the Dahan, but rather _supplements_ that more traditional critique in a sort of silent way: as the game unfolds, the board gets whiter and whiter, and more and more plastic spreads.

We’ve been writing a lot about getting art for _First Edition_ ([here, for instance](https://towergrovegames.com/blog/key-words/)), and I’ve been thinking a lot about components. _First Edition_ is not making the kind of high-stakes anti-colonial intervention that _Spirit Island_ is, but I am invested in following _Spirit Island_ in making components that are not merely instrumental, but suggestive of the larger theme as well. I want the cards in _First Edition_ to look and feel like books that you, the player, get to add to your shop. In the case of _First Edition_, that likely means we’ll be going in a more “realistic” direction with our components. But I wanted to highlight _Spirit Island_ here as a counter-example of a game where anti-realistic components can be meaningful and even immersive in a different way.

I look forward to thinking more about components, art, and meaningful games in this space!

\-- Sam