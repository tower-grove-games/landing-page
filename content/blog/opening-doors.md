+++
author = "Sam"
categories = ["board games", "futurities", "friendship", "exuberance"]
date = 2021-05-21T11:00:00Z
description = "Games -- IN PERSON?!"
lead-image = ""
tags = ["play", "in-person", "virtual", "digital", "production", "playtesting", "design"]
title = "Opening Doors"

+++
Yesterday, I walked through Tom and Lauren’s front door for the first time in 15 months, holding *[Ticket To Ride](https://boardgamegeek.com/boardgame/9209/ticket-ride)* and *[Great Western Trail](https://boardgamegeek.com/boardgame/193738/great-western-trail)*. We didn’t play anything yesterday, but we will this weekend—*[Machi Koro](https://boardgamegeek.com/boardgame/143884/machi-koro)*? *[Pandemic Legacy Season 2](https://boardgamegeek.com/boardgame/221107/pandemic-legacy-season-2)* (too soon?) It’s been a long haul, friends!

In-person gaming is probably the thing I’m most excited about now that Grace and I are vaccinated (or at least the thing I plan to do *regularly* that I’m most excited about). But I’m hoping that the ability to do things in person will open up opportunities for Tower Grove Games as well beyond getting some of our old favorites back to the table at long last. I’m excited to take some time this weekend to spruce up my working prototype of *First Edition* so that Tom and I can play test it in person. I love Tabletop Simulator—I don’t know what I would have done without it this year!—but having the ability to play test in person is going to help us experience *First Edition* in the way it’s designed to be experienced: at a table with good friends and maybe a [Schlafly Tower Grove Park Lager](https://www.schlafly.com/beers/parklager/) or two.

There’s also the matter of conventions. I love conventions, and I’m so excited to approach some that are new to me from the perspective of a company trying to get started. In all honesty, I don’t know if that will happen this year—we aren’t out of this yet, after all, and if 2021’s “con crud” is Covid, I’m going to pass on that—but the experience of walking into my friends’ house yesterday gave me hope in a really solid way that we will be able to experience in-person conventions again soon.

St. Louis has a great board gaming community, and I’m excited to get acquainted with it more fully than I have so far. From [Miniature Market](https://www.miniaturemarket.com/), which houses the [Geekway to the West](https://geekway.com/) board game library, to the various St. Louis board game meetups, to Jamey Stegmaier’s Design Day, which I’ve been able to attend a few times—St. Louis is a great place for the Board Game Hobby.

Here’s to brighter times ahead, everybody. Stay safe out there! We’ll be working hard to develop *First Edition* into something you’ll be excited to play at your friends’ house *with other humans* soon. 

-- Sam