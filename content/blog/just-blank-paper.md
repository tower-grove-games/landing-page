+++
author = "Tom"
categories = ["books", "history", "medieval", "creativity", "generation", "ideas", "board games", "reflection", "explanation"]
date = 2021-06-08T11:00:00Z
description = "Another game about books, on the books!"
lead-image = ""
tags = ["education", "history", "exploration", "passions", "brainstorming", "design"]
title = "Just Blank Paper"

+++
This post-Memorial Day weekend, the fam and I collected our gear together, packed up the car, and took a drive to the middle of nowhere, Tennessee. We set up a tent and an EZ-up, weathered some light rain (in our jackets) and some wicked onslaughts of torrential fury (in the tent), hiked to splendid waterfalls and their swimming holes, and generally got grubby as grub can get. My child was more dirt than human, and he loved it.

Usually, when we go camping, I bring 10 books, read 2 of them, and generally feel like I could spend more time Away From It All. I still feel that last thing, but this trip instead of bringing too many books, I brought 10 or so sheets of blank paper, a couple mechanical pencils -- and not much else.\* Now, I have about three sheets of paper filled with preliminary information necessary for creating a brand new game! 

The game is about medieval scribes and their miscellaneous books, and for now I'm calling it *Codicologica*. [Sam has written](https://towergrovegames.com/blog/the-state-of-first-edition/) already about how important it is to design around your passions, and this little baby game definitely follows his advice. I'm fascinated by scribal labor in the Middle Ages, when print had not yet been invented and every single word anyone ever read had to be written by hand. (In fact, I wrote a dissertation on the topic? Yikes!) 

Privately, I usually think of medieval scribes as playing a game of their own: how do you choose what text to copy next? Once you've decided, where do you copy it from? (How did you hear about it in the first place?) Do you start writing it directly after the last thing you copied, or do you start a new page? What do you do with the space in between? Can you fit two columns on one page (as in many modern Bibles)? How much do you plan ahead for the next text? Are you going to cram as much into one page as you can? Or will you leave plentiful marginal space around the edges for annotations, decorations, and potential re-use? Do you want pretty illuminations to gaze at? Do you double space your lines? What script do you use?

The point of *Codicologica* is to represent many of these real historical problems that real historical people encountered in a form that is (a) accessible to modern gamers and (b) fun to think with. (I guess also: (c) balanced; given what [I've already written](https://towergrovegames.com/blog/on-playtesting-and-design-i/).) I've been rolling over some ideas and tweaks in the back of my head for a while now, and I've described little bits at a time to both Sam and Lauren as they became clear to me; but it's all been pretty fuzzy for a few months here. 

Now, like the medieval scribes I study, I have words on paper, and they aren't perfect, but they're getting there. I'm stoked to find out what kind game I can put together!

-- Tom

\* Ok, I had *one book* because I'm reading Dante's *Divine Comedy* again for the first time since Y1 Grad School, and let me tell you, this thing HOLDS UP.