+++
author = "Sam"
categories = ["ranking", "listicle"]
date = 2021-03-26T12:00:00Z
description = "Sam's play favorites"
lead-image = ""
tags = ["declarations", "mysteries", "dungeon crawlers", "engine-builders", "collaborative games"]
title = "Sam's Top 5"

+++
Unlike Tom, I _like_ ranking things—at least up to a point. I feel fairly confident in my top 5 here, even if things start to get fuzzy after that. So, here they are, in reverse order:

5\. _Chronicles of Crime_: for a mystery-lover, this game really delivers on the dream of taking on the role of sleuth yourself. The app integration is simply amazing, and Lucky Duck Games has proven they can make this system do some incredible—and varied—things.

4\. _Spirit Island_: my favorite board games are all about solving puzzles, and _Spirit Island_ offers one of the richest, most variable puzzles out there. Add to that the huge wealth of spirits, especially with expansions, the adversaries, the scenarios—this game will always have a place in my collection.

3\. _Terraforming Mars_: I don’t know of a game with more viable paths to victory than _Terraforming Mars_. My good friend once said after a game (probably his 15th play): “the great thing about _Mars_ is, I’ve never played that game before.” He meant that the cards that came out shaped that play of the game in a fundamentally different way—and he’s right.

2\. _Everdell_: this game is so well designed, and perfectly scratches my itch for an engine-builder that doesn’t take all day (as a fully-expanded _Mars_ does). Add to that the narrative element—you can see how your city is developing as you populate it with constructions and critters—and the best-in-class art, and _Everdell_ is as close to a perfect production as there is.

1\. _Gloomhaven_: easily my most-played game, and for good reason. The variety of classes—not just the sheer number of them, but the real, tangible differences between them—and of scenarios (95!!!) in the box, paired with the exquisitely simple two-card combat system gave me my first taste of a game I literally could not put down. This game got me into mini painting and got my friends onto Tabletop Simulator, and it holds a special place in my gaming life that will, I think, never be eclipsed.

There it is, my top 5! Let me know what you think of my list, or fill me in on yours in the comments below!

\-- Sam