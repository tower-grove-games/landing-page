+++
author = "Sam"
categories = ["board game design", "response", "reflection"]
date = 2021-04-09T12:00:00Z
description = "Examination over iteration!"
lead-image = ""
tags = ["limitations of human experience", "iteration", "motivation", "playtesting", "design"]
title = "On Playtesting and Design (II)"

+++
First of all, I want to challenge Tom’s characterization of himself as “lazy, selfish, and obsessed with efficiency.” This is 100% accurate when he is, say, playing a game of _Gaia Project_, but much less so otherwise.

With that out of the way, I want to revisit our recurring argument in a public forum. I would say that we have come to an understanding on this question, which is basically this: we need to play test, of course, but—after the first one, at least—there needs to be a _reason_ to do so. That reason could be: we need to see how x element interacts with y element and that didn’t happen in our last game, we tweaked a mechanism and need to see it in action, we need to test this at a different player count, etc.

In board game media there’s a constant emphasis of the importance of iteration to game design. I don’t disagree, and neither does Tom. But there’s a difference between _iteration_ and iteration for the sake of iteration. Yes, playtesting the same version of the game with the same two people _can_ reveal edge cases, but doing an undirected playtest will let you get to those edge cases faster. Yes, you _can_ role play additional players—and we do! But at some point, my brain is my brain and Tom’s is Tom’s. Problems that are hidden within our designs are likely to stay hidden from us, unless we design a play test specifically to stress that thing—or give it to someone else to try.

Now, it’s clear to me that I fall more on the “more testing” side of things and Tom falls on the “more mathing” side. But that’s probably a good thing. Tom is willing to sit down and run the numbers on _First Edition_ to suggest some tweaks to cards that would have taken us dozens of tests to find. And I’m willing to sit in my living room and roll dice while pretending to be 3 different people. Having this balance in our designing partnership will help us make sure our games are, well, balanced. And we’re excited to share them with all of you to help us make sure that they are fun as well.

\-- Sam