+++
author = "Tom"
categories = ["design", "value", "math", "breakdown", "explanation", "exposition", "auction games", "hobbyist games", "tabletop games", "board games"]
date = 2021-07-06T11:00:00Z
description = "Book math for books not about math."
lead-image = ""
tags = ["first edition", "books", "math", "price", "cost", "value", "theory", "playtesting", "design"]
title = "*First Edition* Breakdown: Card Values"

+++
_First Edition_ [is a game](https://steamcommunity.com/sharedfiles/filedetails/?id=2200689434&searchtext=first+edition "this is an old version, play at your own risk") about collecting rare books and selling them to collectors. You buy low (you hope), and sell high (you hope), and try not to do the other thing where you run out of money, which also acts as victory points. [Sam originally designed this game](https://towergrovegames.com/blog/the-state-of-first-edition/), and now I've been developing it as best as my opinion-burst brain knows how. So this week is a technical post about how I mashed together a loose approximation of value for each card in the deck.

There's a lot of [cool stuff going on](https://towergrovegames.com/blog/the-state-of-first-edition-ii/) in _First Edition_! We have employee abilities, bonus goals, auction tokens, and protective measures! I will not rehearse the rulebook here! But for this post to make any sense, you need to know a bit about how the game works. The important thing is that the vast majority of events are auctions, where players bid against each other for books.

Here's what you need to know. Each book you can collect is represented by a card, and there are 135 book cards total. You shuffle them in setup and draw them over the course of the game, for purchase and sale. These cards contain seven (7) pieces of essential information:

* Author
* Title
* Auction Price
* Store Price
* Era
* Genre
* Era bonus
* Genre bonus

Basically, when you purchase a card, you are going to pay _at least_ the "Auction Price" and when you sell it -- or when another player buys it from you -- you are going to receive at least the "Store Price." If you sell five books from same era, you also receive the era bonus for each of those books. Same for genre, but the threshhold is four. You cannot sell a book for its era and genre value at the same time. The game's mandate is set collection: the way you go from having less money to more money is by collecting cards of a single category and receiving their bonuses. (Buy low-ish, sell high-er!)

![Rough depiction of information depicted on a single card. Not for official use or publication.](/uploads/11.png "A Card Mock-up")

So my questions were: how much is a card worth? what are the most valuable cards? what are the least valuable cards? what is the average card value? how much variation is there away from that average value? are any cards, eras, or genres _too good_? or _too terrible_?

Basically, how do you quantify value when most player interactions are auctions -- which is to say, the value of a given card will change, game to game, players to players?

This is what I did, mostly using Python (3.9...):

* Loaded relevant information about the cards into a list of dictionaries, where each item was a card and each dictionary stored the pieces of essential information noted above.
* Counted how many cards there were for each era and genre.
* Calculated the probability, for each card, of its being drawn and used in a set over the course of a game. I used a 4p game as my basis originally -- approximately 90 draws -- but later on, I checked this for lower draw counts as well.
  * For the math-y folks, the code looked like this:

    ```python
    # calculate number of cases in which era bonus is relevant
        for i in range(5, 46):
            if d >= i:
                x += math.comb(45, i)*math.comb(90, d-i)
    
    # calculate number of cases in which genre bonus is relevant
        for i in range(4, gno+1):
            if d >= i:      
                y += math.comb(gno, i)*math.comb(totalcards-gno, d-i)
                
        ev = x/math.comb(totalcards,d)*(d/totalcards)
        gv = y/math.comb(totalcards,d)*(d/totalcards)
    ```
  * This is for a single card. "d" is how many cards are drawn over the course of the game.
  * There are 45 cards in each era; "gno" is the number of cards in the relevant card's genre.
  * If this math does something wonky and you see that, please please let me know! I made it several weeks ago, and I should have left me better comments, and now my brain is ELSEWHERE.....
* Gave each card two rough expected values (EVs), the first for its Era (based on ev) and the second for its Genre (based on gv).
* Applied to each card an _extremely rough_ "Standard Profit Margin," using the formula:

  > SPM = (Store Price - Auction Price) + Era EV + Genre EV

  This is going to be an over-estimation for two reasons! First, because you can't cash a card for both bonuses at once. And second, because the play of the game tends to drive card prices higher than the listed auction price -- meaning the first parenthetical is just as often negative as positive.
* Found the average SPM of all cards.
* Ranked cards according to their SPM's.

I said it before, I'll say it again, these are all _extremely rough_ considerations of value, and none can account for the play of the game itself. But they do give a standardized sense of how valuable cards are, in relation to one another (I think!). Which is important for the next bit of maths that needed to be done, which I will talk about more, later, because this post is long and intimidatingly text-block-ridden.

\-- Tom