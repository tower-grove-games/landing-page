+++
author = "Tom"
categories = ["audience", "rulebooks", "technical writing", "writing", "revision", "design", "tabletop games", "hobbyist games", "board games"]
date = 2021-07-20T11:00:00Z
description = "When you write, you use words, but also you use *feelings*."
lead-image = ""
tags = ["mechanics", "theme", "restraint", "ambition", "quidditas", "vibe", "clarity", "rulebooks", "writing", "design"]
title = "Writing, Rules, Vibes"

+++
You may have realized by now, dear imagined audience, that Sam and I are both literature people, doing doctoral sorts of things with words and concepts in the vasty realms of English and English-language textual production. So I've been poking around at quite a few jobs that revolve entirely around the question of _writing_. What is good writing? What aspects of good writing are a matter of skill? What aspects are a matter of intuition? How do you pinpoint the components of good writing? How do you teach the recognition of good writing? How do you turn not-so-good writing into good writing? What are the pitfalls of revision, creation, imagination, rigor, and flexibility in the production of this preposterous communicative tool we call language?

[_(Buffalo buffalo buffalo buffalo . . . )_](https://en.wikipedia.org/wiki/Buffalo_buffalo_Buffalo_buffalo_buffalo_buffalo_Buffalo_buffalo)

I am used to asking these questions about academic writing, because that's my profession! If you want to make an argument, there are more and less successful ways of going about doing that. I can help you sort through those, it's a thing I do.

![](/uploads/editsinred.jpg)

Of course, many good writing techniques and principles port into technical writing, as well -- and I would count board game rulebooks as a kind of technical writing. The purpose of a rulebook is to get people playing your game! The most important thing is that it develops a coherent vision that players can understand and refer to.

![](/uploads/clankrulessnap.jpg)

But there are hidden affordances in rulebooks apart from technical clarity! A good rulebook slowly introduces component parts and mechanics, establishes a voice (and so: a tone, a feel) for the game, makes the essential qualities of the game evident before any player reaches for their pawn, deck, or dice.

![](/uploads/jotl_rules_snap.jpg)

I get the sense that, in the board game industry, most conversations revolve around whether or not the rules in a rulebook are sufficient (in language, order, reference, etc.) to explain the game. I wonder, however, whether there's more room for a larger conversation about rulebooks that participate in the game itself -- that are invested in, rather than a mere reference to, what the game is invested in.

![](/uploads/wingspan-rules-snap.jpg)

That's a lot to ask! In my experience, an excellent rulebook forms an essential part of its game's _vibe_ -- the _quidditas_, the _je ne sais quoi_, the whatever-it-is that the game _is_. But maybe it's not a rulebook's job to be excellent! I certainly can think of massive successes, and I'm sure we all have our own opinions about what count as failures. I wonder whether the failures fail because they shot for the stars or because they tried to play it safe.

![](/uploads/power-grid-rules-snap.jpg)

My question for this week, and the thing I'd like to pay attention to as I try out more and more games, is:

What rulebooks best integrate the _vibe_ of their games? What rulebooks, well . . . don't? Do they get points for trying?

\-- Tom