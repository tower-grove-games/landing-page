+++
author = "Sam"
categories = ["update", "design", "overview"]
date = 2021-04-02T12:00:00Z
description = "first report on First Edition"
lead-image = ""
tags = ["playtesting", "balance", "auctions", "passion", "design"]
title = "The State of *First Edition* (I)"

+++
I started working on _First Edition_ in early Spring of 2019. Elizabeth Hargrave’s _Wingspan_ had just come out, and although _First Edition_ bears no resemblance to _Wingspan_ whatsoever, Hargrave’s game helped make the point that there are _lots_ of untapped themes in the Board Game industry. She made a game about birds, one of her passions, and it turns out that a _ton_ of people really like birds! So I sat down and made a list of topics that I have a) specialized knowledge about and b) a passion for. As a PhD student working with 19th and early-20th century literature, “old books” was right at the top of that list.

All of this is to say that _First Edition_ is very much a theme-first design. As the good folks who patiently soldiered through a few of the early iterations of _First Edition_ at the 2019 Stonemaier Design Day and at my own various family reunions could attest, the theme has always been strongly in place; it’s the mechanics that needed some work. With the help of those play testers, however (and with some very generous feedback on the design at the 2020 virtual Stonemaier day as well), I feel that the mechanics are now set. The result is a fast-paced auction game with enough euro elements to give it some strategic depth, but not too much in the way of rules overhead or extreme cognitive load.

My approach to this design has always been to start by approximating numbers until the mechanics are in place, and then move to the work of making sure nothing is wildly out of balance. One benefit of Auction games is that the players themselves set the market: cards are worth what the players think they are worth, no matter what they say on them. However, it’s still important to have a game that allows players to be competitive even if all of the auctions are won with the lowest possible bid. Reader, let me tell you, I’m thrilled to have Tom’s Math Brain available to help me with this aspect of _First Edition_. Of course there are lots of playtests ahead to make sure there are no edge cases that are utterly disastrous, but I know that having a solid mathematical basis undergirding it all is going to make _First Edition_ really stellar. I’m excited to keep you posted as the design gets closer and closer to being out in the world!

\--Sam