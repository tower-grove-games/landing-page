+++
author = "Sam"
categories = ["appreciation", "narrative", "reflection"]
date = 2021-04-16T11:00:00Z
description = "Sam works out with cardboard"
lead-image = ""
tags = ["organizing", "value", "storage", "collecting"]
title = "Heavy Games"

+++
This weekend, I learned exactly how heavy our 110ish game collection is. We moved! And I got to carry all of our games.

We hired movers, but our game collection made the list of things we wanted to move by hand. This seemed perfectly natural to me until my mother-in-law asked why we didn’t just let the movers move those heavy boxes. Well, I said, we don’t want them to accidently drop a box and damage it. That’s true of anything--dishes, picture frames, you name it--she replied, and of course she was right.

![](/uploads/heavygames_1.jpg)

But the real reason I wanted to move our games by hand is that they are some of our most prized possessions. If movers drop a box of dishes, you can get new dishes. In a limited sense, that’s true of games, too. But what about our all-in _Tainted Grail_ pledge? Our mostly complete copy of _The 7th Continent_? The _Gloomhaven_ and _Scythe_ boxes that are full of minis it took me hours upon hours to paint? I’m not entrusting those things to movers, even if they are just plastic and cardboard at the end of the day.

The other thing that moving has made me think about in relation to games is shelving, storage, and the way your board game display can shape the feel of your living space. My wife pointed out that during the three years we lived at our last place, our collection grew steadily, but haphazardly. We would get a new game and then find a place to put it; rinse, repeat. But now, we have the chance to organize our current collection from the ground up! We’re still limited by the shelves we happen to have, but moving gives you a new opportunity to think about your games in a new way. Check out the picture below to see what we ended up doing! We are thrilled to be able to have our entire collection in one place.

![](/uploads/heavygames_2.jpg)

Both of these anecdotes sort of connect for me in this way: moving has made me think (not for the first time) about the parts of this great hobby that extend beyond the 30-180 minutes I get to spend playing a game with my wife or my friends. Having our games all displayed in our new living room really ties the room--and the apartment--together. And my instinctive protectiveness over them brings home to me how big a part of my life hobby board gaming has become.

Best wishes to everyone out there who has grown to feel this way about our hobby! I’ve got to go unpack some more boxes…

Until next time—Sam