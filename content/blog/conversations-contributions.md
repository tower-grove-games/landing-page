+++
author = "Sam"
categories = ["comparison", "motivation", "reflection"]
date = 2021-04-23T12:00:00Z
description = "Sam is writing a dissertation and also making board games"
lead-image = ""
tags = ["dialogue", "conversations", "twists", "mechanics", "community", "design"]
title = "Conversations / Contributions"

+++
This has been a big week at Tower Grove Games. We’ve had some important and fruitful discussions, both internally and with some new friends in the industry; learned a lot about copyright law with regard to images produced in the eighteenth century; and horribly misplayed a 2-player game of [_Pax Pamir, 2nd Edition_](https://boardgamegeek.com/boardgame/256960/pax-pamir-second-edition).

Throughout it all, I’ve been hard at work on the first draft of my introduction to my dissertation. I won’t bore you with the details, but what I want to write about here is my inescapable sense of the similarities between my work this week on the diss and our efforts to become more immersed in the board game industry. In academia, we talk a lot about contributing to a critical conversation. The reason we have to read and cite other scholars is both to show that we’ve done our due diligence—assured ourselves that no one has said quite the thing we are saying about any given work or topic—and to help other scholars see how the new thing we are saying fits into the field.

It’s struck me this week that board game designs are doing the same thing, even if the stakes are different. When a game like [_Dune: Imperium_](https://boardgamegeek.com/boardgame/316554/dune-imperium) comes out, for instance, it’s situated in a specific context within the industry, defined both by its mechanics and its theme. [The designers have written](https://www.direwolfdigital.com/news/dune-imperium-designer-diary-1-beginnings/) about how they designed _Dune: Imperium_ with _Dune_ (the board game from the 70s) in mind, paying particular attention to making _Imperium_ feel very different from the earlier game. At the same time, the mixture of deck building, worker placement, and combat/area control puts the game in conversation with other games that use those mechanics—including, of course, [_Lost Ruins of Arnak_](https://boardgamegeek.com/boardgame/312484/lost-ruins-arnak), which miraculously came out at the same time.

This is another way of looking at games offering “twists” on things (a “twist” on deck building, a “twist” on worker placement). You have to design in this way—we don’t want to play the same game again and again, we want to see the newest innovation. But shifting the perspective to think in terms of conversations can be fruitful, too.

Ultimately, the main reason we talk about critical conversations in academia is to emphasize that you are a _contributor_, in dialogue with other people: you can’t just run off screaming your own opinions about things. No one likes that person, and I don’t want to _be_ that person. What I _want_ to be is a contributor to the amazing conversations that are happening in board games. At the end of the day, conversation is what creates community, and community is what we all want at Tower Grove Games.

\--Sam