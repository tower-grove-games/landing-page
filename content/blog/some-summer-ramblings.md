+++
author = "Sam"
categories = ["collecting", "enthusiasm", "design", "play", "board games"]
date = 2021-06-11T12:00:00Z
description = "Normal Sized Enthusiastic Sam Discovers Tiny Epic Dinosaurs"
lead-image = ""
tags = ["fun", "structure", "goals", "collecting", "discovery", "play"]
title = "Some Summer Ramblings"

+++
It’s been a pretty relaxed week at Tower Grove Games. My new apartment complex has a pool, so we had our weekly meeting there, which was fun. We mainly talked about the new design Tom is working on, and we’re excited to share more details of that as they come more into focus.

The big gaming event in the Smith-Lillard household doesn’t really have anything to do with Tower Grove Games, but I’m going to talk about it anyway. As a birthday present toward the beginning of the year, I got Grace a weekly board game calendar from [boardgametables.com](https://www.boardgametables.com/products/2021-board-game-calendar). Besides having great photos of a different game each week, the calendar gives you space to record your 10 x 10 challenge and other data about your gaming habits over the course of the year. The best part, though, is that it gives you a weekly challenge: “play a game with a medieval theme,” “play a game without learning the rules first,” etc.

This week’s challenge was “play a Tiny Epic game.” Well, we don’t have a Tiny Epic game. Usually, when we can’t complete the challenge, we just move onto the next one. But I got curious about [the Tiny Epic series](https://www.gamelyngames.com/). I’ve seen them at conventions, etc., but never played one—or even set one up. After some quick BGG research, I decided that our collection could probably benefit from a tiny dinosaur game, so we ordered [Tiny Epic Dinosaurs](https://boardgamegeek.com/boardgame/291508/tiny-epic-dinosaurs)—which happened to be the calendar picture as well!

The game arrived Sunday afternoon, just in time. Let me tell you: they are *not* misusing the word “tiny!” This is going to make for a perfect game to play at a brewery: it will fit in a pretty small bag, setup is easy, playtime is relatively short, and it obviously has enough depth to warrant multiple plays. All told, Tiny Epic Dinosaurs strikes me as a game that is doing exactly what it sets out to do: it fills that thematic and literal, physical niche in our game collection, has amazing tiny components, and packs quite a punch in such a small package.

It’s also got me thinking again about the joy of buying new games. As hobbyists, it’s always fun to try new games—on TTS, for instance, or at a convention. But buying and thus owning a new game just scratches a different itch.

What’s the newest addition to your collection that you’re most excited about? Have you tried any of the Tiny Epic games?

Thanks for reading my scattered thoughts and look out for more on Tom’s new game next week, as well as a *First Edition* update. Happy summer!

--Sam