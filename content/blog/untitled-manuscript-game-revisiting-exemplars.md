+++
author = "Tom"
categories = ["exemplar", "strategy", "revision", "playtesting", "design", "hobbyist gaming", "table games", "tabletop games", "board game", "board games"]
date = 2021-08-17T11:00:00Z
description = "Points are good, but games are better."
lead-image = ""
tags = ["theme", "alpha", "play", "play-and-write", "medieval", "exemplar", "probability", "setup", "strategy", "components", "revision", "competition", "interaction", "player competition", "player interaction", "playtesting", "design"]
title = "*Untitled Manuscript Game*: Revisiting Exemplars"

+++
So a bad thing happened in one of the early playtests for *UMG*. Overall, if I can be forgiven for a small bit of self-praise, these playtests went really well! Players felt a certain friction between the pressure of acquiring texts in a coherent pattern and copying texts as quickly as possible, cards circulated around the table relatively well, and it was obvious that there were multiple viable paths to victory, depending on points earned from individual texts, collections of like texts, and possible layouts -- as the pros call it, *mise-en-page*.

![](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Tables_and_treatises_on_the_computation_of_the_calendar.._Wellcome_L0073777.jpg/640px-Tables_and_treatises_on_the_computation_of_the_calendar.._Wellcome_L0073777.jpg)

But Lauren discovered a bit of a bug in one match where we saw an extremely high percentage of prose texts come to market at regular intervals across the course of the game. There are currently eight (8) prose texts out of sixty-one (61) cards total in the whole game; and we saw six (6) of them appear in the first 25 draws are so. This is unusual but, obviously, not impossible; and in fact my weak grasp of probability and distributions says if it didn't happen at least some of the time, that would mean I was designing a game that broke the laws of reality and happenstance. Thank goodness it happened! Someday, someone will flip all eight prose texts during setup, which calls for ten (10) intial cards to be available, and all will be right with the universe.

![](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Ellesmere_Manuscript_Knight_Portrait.jpg/640px-Ellesmere_Manuscript_Knight_Portrait.jpg)

The problem is, Lauren decided to simply copy all of the highest-valued texts that she could and entirely avoid going for Exemplar bonuses in the center of the table. This is especially valuable with prose texts because they allow for the most flexibility in deployment. It was comparable to a game of *Splendor* where no one gets a noble -- except, at least in every competitive group I've ever played with -- no one really goes for nobles in *Splendor* anyway, because the most efficient path to 15 points doesn't require it. (I'm sure there's *Splendor*-meta that would demand such flexibility, let's stay on topic here!) I want this to be a *viable* thing for Lauren to do, but I don't want it to be *consistently the best strategy* in every play. If you ignore the main interactive mechanic of the game once, fine! If it's the best thing to do most of the time -- yikeses. Not good! I don't want *UMG* to recreate the *Splendor*-dynamic (as I have experienced it).

![](https://cf.geekdo-images.com/woLWYlyek5wuVU_9PEJxgA__imagepage/img/-P7-TxmWexxluSziRLzS1UsYXlg=/fit-in/900x600/filters:no_upscale():strip_icc()/pic2020178.jpg)You might say: "But Tom, that was one playtest, aren't you overreacting?" NO! Because, again, that tenuous grasp of probability lurks in my mush-brain, and it's pretty sure those eight (8) prose texts, along with some of the more valuable poems, will appear often enough in the first X relevant draws to make Lauren's strategy at least *apparently strong* in every play -- which re-introduces through my design choice a problem that could arise only by chance. Oh no!

![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3f/Tables_and_treatises_on_the_computation_of_the_calendar.._Wellcome_L0073768.jpg/640px-Tables_and_treatises_on_the_computation_of_the_calendar.._Wellcome_L0073768.jpg)

The question was: what strategies did I want to make competitive with the Ruthless Lauren Approach (RLA)? And how could I encourage greater interaction between aside from competing to enact that approach more efficiently?

The result was a full overhaul of how Exemplar Cards are scored. In my original conception, you had to complete every poem in a given manuscript to score points for it at all. But it's *hard* to find every text in a given manuscript and *harder* to copy all of those texts in the time it might take the RLA to just cram in as many points as possible and end the game.

![](/uploads/revised_exemplar_0.jpg)

Now, I've given the opportunity for partial completion for each Exemplar -- meaning you can score more than one, and you can get variable points for how many texts you complete. I determined these points using some funky python modules (ok, it's just PANDAS over and over), and Sam made brand new cards for me to try out with those values. What fun!

More details later, after more playtests!

\-- Tom