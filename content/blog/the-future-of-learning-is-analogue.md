+++
author = "Tom"
categories = ["vision", "values", "mission"]
date = 2021-03-22T17:00:00Z
description = "the vision from the present"
lead-image = ""
tags = ["design", "overview", "in development"]
title = "The Future of Learning is Analogue"

+++
Here at Tower Grove Games, we believe that learning can be fun -- and that fun can be learned in a thousand unexpected ways. As educators, we see a world of potential for the use of board games and gamified learning at home and in the classroom. We hope to make some of that potential a reality as soon as we can. As enthusiastic hobbyist gamers, we see a tight-knit community of creative, passionate, dedicated, brilliant, and beautiful humans designing the games we love. We hope we can jump right into that community, supporting, refining, and appreciating all manner of projects in any way we are able. We want our games to be immersive, intuitive, and enjoyable for all audiences.

In our dual-guises, as educators and gamers, we are working on a slate of games that correspond to the breadth of our interests. Currently in development are:

* a lightweight, fast-paced, dice-drafting game low on instructions and deep in strategy, designed to develop math sense for subsequent encounters with complex problems in probability and combinatorics;
* a lightweight, high-variable, customizable deck-building game with as much variability as the English language has to offer;
* a collaborative, multilayered, storytelling extravaganza, and an ode to one of the greats -- and one of our favorite authors -- Charles Dickens;
* a mid-weight auction game themed around book publishing and book collecting in the Victorian and Modern eras;
* a mid-weight pattern-efficiency game designed to imitate the complexities of making manuscripts in the Middle Ages;
* a midweight two-player map-building game inspired by Milton's _Paradise Lost_;
* a heavyweight, card-based, woodland-themed crawler with variable class-dynamics and multiple storylines.

So we have a lot of work to do, and we're delighted to report on how much we've done in the last year. We are absolutely thrilled to be launching this blog alongside our new website, as a place to discuss what we're up to, why we're up to it, and when all that up-to might turn into games you can plunk down on your table and play.

In this space, we'll also discuss some of our favorite games to play, the resources that have helped us most, and the people who have pioneered the path we take.

Send us an e-mail, @ us on Twitter, and tell us about _EVERYTHING_, because we are here to learn and here to play! Thanks for reading and thanks for playing!

\-- Tom, on behalf of Lauren and Sam