+++
author = "Tom"
categories = ["wordplay", "reflection", "update"]
date = 2021-04-20T12:00:00Z
description = "Because our game has one name, it does not have other names"
lead-image = ""
tags = ["woodland creatures", "audience", "market", "puns", "naming", "design"]
title = "The Name of the Game -- Forest Feast: A Dice Drafting Game"

+++
Once more, our first lightweight game in development has been given a name! To the great disappointment of the reigning toddler at _Tower Grove Games_, this name has zero rhymes. It does alliterate, though, which is something; and it's evocative, which is another thing; and -- the best thing of all -- it does not already have an entry on _Board Game Geek_ dot com. It also, I should say up front, has nothing to do with giraffes. _YET._

So, as of last week, this game bears the title: _Forest Feast: A Dice Drafting Game_. It's not set in stone, especially the subtitle, but it does have a ring to it, no?

With a bit of touching up of art and design, and a wee bit of messing about on Tabletop Simulator, we should have a test/PnP copy freely available within the next few weeks. We're excited for you to give it a go! And we're especially excited to hear what it reminds you of, and what other dice games it feels like.

That said, it does seem a bit premature to run through the rules, conceit, and educational benefits of the game until it's playable in some form, as I once thought to do in this post.

So for now, in the spirit of sharing the abundant creativity of the whole team, I thought I'd detail some rejected or sidelined names we came up with. If any of these strike your fancy, or if your marvelous word-brain hoards another suitable suggestion -- post below! Or send us an e-mail! Or hit us up on Twitter or Insta!

* Hungry Hedgehogs
* Critter Cache
* Critter Cravings
* Badger Banquet
* A Feast in the Willows
* Forest Festival
* Ravenous Giraffes
* Girafvenous
* Jumbo Giraffe Gumbo Gamble
* Jammy Giraffe Jamboree

Anyway, that's all from me this week. Keep an eye out for my first childrens' book, _Jumbo Giraffe Jamboree: Jimmy Giraffe Makes Jam in a Pajamas_ along with its sequel, _Jumbo Giraffe Jamboree: Jenny Giraffe Jumps into a Jumble of Gems_. What other titles belong in this very real, totally not made up series about larger-than-usual giraffes and their exploits in/near/around jungles?

\-- Tom