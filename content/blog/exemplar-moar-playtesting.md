+++
author = "Tom"
categories = ["play-and-write", "medieval", "exemplar", "playtesting", "revision", "fun", "play", "design", "hobbyist gaming", "table games", "tabletop games", "board games", "board game"]
date = 2021-08-24T11:00:00Z
description = "There are a lot of words for books, and now this game uses one of them, which is spiffy."
lead-image = ""
tags = ["play", "play-and-write", "medieval", "alpha mod", "competition", "scoring", "points", "texts", "manuscript", "exemplar", "revisions", "playtesting", "design", "fun"]
title = "*Exemplar*: Moar Playtesting"

+++
A hefty chunk of Tower Grove Games is in California right now, getting in some long-delayed family time on account of this whole pandemic situation -- and no doubt the remaining chunk would happily be here as well. The wine's good, the food's good, the weather's not at all swampy, and best of all, I get to torture my family and friends and family-friends with the newly printed Alpha Mod for the game formerly known as _Untitled Manuscript Game_ but now going by the title: **Exemplar**.

![](/uploads/exemplar_ca_fun_1.jpg)

"Exemplar" is a fun word, and if you get on the wrong side of the wrong very smart humans, they will explain to you exactly why it's not exactly an accurate word for what's happening in my game. To play _Exemplar_, one need not imitate layout, order, or handwriting (in this case: squiggles) from a wholly complete book or book-bit.

![](/uploads/exemplar_ca_board_1.jpg)

In fact, the game works more the other way around: players collect book-bits (or, as some normal humans call them, "texts") from a common pile and use those book-bits to construct a pre-existing set of book-bits (or, as some medievalists might say, "compilation"). I originally called these collections "Manuscripts," which is a correct sort of phrasing, but correct with not enough pizzazz.

![](/uploads/exemplar_ca_fun_2.jpg)

But common consensus is that _Exemplar_ sounds much, much cooler than _Manuscript_ or _Compilation_ or _Collected Book Bits_, and since this is my board game, not my scholarship, I figure we're close enough to the idea of the thing to make the title good. We sat on it a few weeks, and here we are.

![](/uploads/exemplar_ca_board_2.jpg)

Anyway, as the photos I'm shamelessly scattering through this post will show, everyone had just a fabulous time finding bugs, glitches, and unfortunate phrasings in the game's deployment, rules, and explanation. They made very pretty books with their alpha mod! I learned quite a bit about how to explain my game, and I made some major quality of life adjustments to the cards, which I'll explain next week. Colored pencils were involved!

![](/uploads/exemplar_ca_fun_3.jpg)

Overall, my valiant relatives agreed with Sam and Lauren's impression that the basic blocks of the game tick along well. Teddy destroyed the competition in the first game, and Grams eked out a win in the second game (they played twice! on purpose!) so I'm including those winning sheets here . . .

![](/uploads/exemplar_ca_win_1.jpg)

and also here:

![](/uploads/exemplar_ca_win_2.jpg)

The best news is that I still haven't won at my own game. Way to go me! Great job! More about the tweaks and quality of life improvements next time.

\-- Tom