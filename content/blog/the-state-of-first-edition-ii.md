+++
author = "Sam"
categories = ["plan", "intention", "reflection", "design", "board games"]
date = 2021-06-25T12:00:00Z
description = "First Edition is good, but it could be better."
lead-image = ""
tags = ["active abilities", "passive abilities", "auctions", "refinement", "fun", "mechanics", "playtesting", "design"]
title = "The State of *First Edition* (II)"

+++
Last night, we got the chance to playtest _First Edition_ on a real table! We played a 3-player game, and it took about an hour and a half, after the rules teach. I had a great time, and I learned a lot about the game and where it’s at in its development.

First, the good: the book cards are working really well. The level of strategy in the set-collection introduced by the genre sets is just enough, I think, and the auctions were fun: there was nearly always at least one player who was invested in one of the books in the auction. I also think that the starting amount of money is right: the economy is tight enough at the beginning of the game now that you can’t just throw money at every auction; you have to pick your spots early and then eventually you can take some more risks. That’s right where I want it to be.

Next, some points that can be improved. We had an unfortunate setup in this one, in which:

1. Most of the employees in the market interacted with the discard pile
2. Many of the event cards interacted with the employees and
3. We weren’t putting any cards in the discard pile!

Only 1 employee got hired during the game, and so that aspect of the game just wasn’t there. Add to that the fact that many of the events dealt with employees, and we had some turns where not a lot happened.

We want to address this issue in two ways, neither of which will be an _over_-reaction, I think. First, we want to make the discard pile into more of a factor, mostly by having you draw more cards during events and making you discard a certain number of them. This will increase your decision-space on your turn, which is fun, while also allowing more interaction with the discard pile because it will, well, _exist_.

The second thing we want to do is work quite a bit on the employees. We think there can be fewer of them per game, and that we can put them into tiers. So we will have a subset of employees that would form the initial market (i.e. you would randomly draw 5 out of a set of 15 or something to be the starting set for each game) and then a different subset (say, 10 out of 20) to be the rest of the employee deck. The starter employees would all give you smallish advantages that would still allow you to specialize, with the added benefit that you would start flipping over more powerful employees if you hired more of the starters. I’m excited to work more on this system, which I think will make the employee mechanics more robust without being necessarily more complicated.

All in all it was a great play test, with lots of things to think about going forward. We’ll tinker with these things and get it to the table again soon. After all, it’s all about iteration!

Until next time,

\--Sam