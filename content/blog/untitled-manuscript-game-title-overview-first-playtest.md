+++
author = "Tom"
categories = ["editing", "development", "modification", "playtesting", "play", "design", "game design", "table games", "hobbyist games", "tabletop games", "board games"]
date = 2021-08-03T11:00:00Z
description = "You can write with your hands, but you have to think with your brain."
lead-image = ""
tags = ["development", "play-and-write", "modifications", "play", "playtest", "playtesting", "components", "alpha", "theme", "design"]
title = "*Untitled Manuscript Game*: Title?, Overview, First Playtest"

+++
[NanDecked](http://www.nandeck.com/) and [Pandoc-ked](https://pandoc.org/), we put the very first black-and-white, aesthetically inept, bare-bones, mechanical _Handschrift_ alpha v. 0-1 of _Untitled Manuscript Game_ on the table on last week, and wow did the playtesting go better than I had hoped! Lauren and Sam were extremely patient with my rules run-down, and both found small loopholes I hadn't considered. But overall, the game is working about the way I had anticipated! The majority of my work before the next playtesting run (and then -- [Stonemaier Design Day](https://store.stonemaiergames.com/products/stonemaier-games-design-day-2021)!) has to do with tinkering point values for both types of bonus cards and modifying aspects of flow. I may add or subtract a text or two here and again, but I'm also fairly satisfied with how the distribution panned out. _If it ain't broke_ . . . .

We have also been thinking about new names (since publishing as _Untitled Manuscript Game_, while cute and reminiscent of delightfully malicious waterfowl, sort of doesn't capture the game's spirit)! I tried _Codicologica_, which Sam and Lauren despised; then I suggested (from my research) _Booklet Theory_ -- which was equally poorly received! Right now we're considering _Exemplar_ or even _Exemplaria_ (if the [journal of the same name](https://www.tandfonline.com/toc/yexm20/current) will forgive us our admiration). Further suggestions welcome!

I'll post the rules to our wobsite soon! But they are (a) ugly enough and (b) incorrect enough in their current state to merit a bit of holding off at this time. So here's a sort of mini-photo-journal of our very first playtest, in which I lost miserably at my own game!

First, the setup! 10 text cards are drawn to start the game in parallel rows of increasing cost, in the style of _Pax Pamir_ or _Through the Ages_. 5 shared manuscript cards are also drawn: for completing these, players get bonus points! These can be called "exemplars" -- hence the proposed name of the game!

![](/uploads/ptest-1-1.jpg)

Then the game is played, according to the rules which you will be able to read whenever I get around to editing them. But basically, you acquire texts, copy them into your manuscript, then hope someone will pay you for them once you're done. We used _Wingspan_ cubes for actions (which have variable costs!) and a lot of cast-off printer paper for trial-run grids and game boards!

![](/uploads/ptest-1-2.jpg)

At the end of the day, Lauren's "only copy high-point value texts" strategy won out, followed by Sam's completion of Titus A XX. Tom's attempt to complete Titus A XX (piggybacking off Sam's dedication) plus a bonus text worth a bunch of points did not pan out. Be dedicated!

![](/uploads/ptest-1-3.png)

Anyway, I'm bummed to get destroyed at a game of my own making, but at least I completed the twelfth century's absolute best text, Walter Map's _De nugis curialium_! Thanks for checking this out.

\-- Tom