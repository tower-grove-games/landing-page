+++
author = "Tom"
categories = ["public domain", "theory", "description", "resources", "list", "wrap-up"]
date = 2021-05-18T11:00:00Z
description = "There are pictures on the internet"
lead-image = ""
tags = ["repositories", "images", "public domain", "resources", "aesthetics", "art", "cover art", "design"]
title = "Copyright, Cover Art, and the Idea of Property (IV)"

+++
Ok, next week, I promise not to write about copyright. This is the fourth entry in what became a four-part series of Tom not being a lawyer or giving legal advice and also trying to understand things out loud. You can find [Part I here](https://towergrovegames.com/blog/copyright-cover-art-and-the-idea-of-property-i/), [Part II here](https://towergrovegames.com/blog/copyright-cover-art-and-the-idea-of-property-ii-hundreds-year-old-images-under-copyright/), and [Part III here](https://towergrovegames.com/blog/copyright-cover-art-and-the-idea-of-property-iii/).

I'm going to skip the re-cap, because, well, I re-capped last week, and also, I defended a dissertation on Friday, and I'm still sort of tired in the brain-region. So, as promised, some links, etc.

If there's a sort of opinion-spectrum about copyright, it generally might be contained between two poles. On one end, you'd have a Lessig-style Copyright Is Mostly Bad and Unhelpful For Humans, for which perhaps the most enthusiastic (and endearingly written) account I've run into is [Cory Doctorow's _Information Doesn't Want to be Free_](https://openlibrary.org/books/OL25905303M/Information_doesn't_want_to_be_free). On the other end, you'd have something like Copyright Is Absolutely Necessary and Should Stand In Perpetuity Like Any Other Created Thing, for which almost certainly the most vigorous (and loquacious and rather grumpy) account I've run into is [Mark Helprin's _Digital Barbarism_](https://openlibrary.org/works/OL14969609W/Digital_barbarism?edition=digitalbarbarism00help). You could also read, like, a [caselaw textbook about copyright](https://openlibrary.org/works/OL18176W/Understanding_copyright_law?edition=understandingcop00leaf), but where's the fun in that?

Like any good millenial academic and avid [reader of xkcd](https://xkcd.com/239/) (well, at least one of those, probably), I'm more sympathetic to the Creative Commons, Doctorow-y folks on the whole. This whole experience with trying to find a 300-year old picture of _Robinson Crusoe_ I'm allowed to sell for money has led me to believe that maybe even most CC restrictions might end up a bit too much, in the long run. Then again, I met Mark Helprin one time, and if he wasn't exactly nice, he was obviously charming and intelligent and sincere. And maybe "the future of civilization itself" (book jacket quote) _does_ depend on every _Flickr_-user's photos being possessed by them and their heirs unto perpetuity, who knows? What I want to know is how the British Library (or Superstack, or whatever) got Defoe's copyright, given that not even Defoe could get Defoe's copyright -- a fun story for another time. (It is the year 2350, and every image ever made is owned by the British Library.....) But I digress.

Anyway, here are some websites I have found that have images you might use in board games, if images are what you need. For the vast majority, you'll need to toggle on a restriction for commercial use, or public domain, or something along those lines; and I'll stress -- less from experience than general fear -- that for every single image you want to make very sure it is actually not copyrighted before actually using it:

* [Flickr](https://www.flickr.com/) is an amazing resource! Though I'll note if someone takes a picture of an image that is certainly copyrighted and then you use that picture for to sell things, the person having the copyright on the OG image might be grumpy.
  * Bonus: the [British Library has its own Flickr stream](https://www.flickr.com/photos/britishlibrary/albums) , which is full of good things! (On desktop, search within stream by navigating to "Photostream" then clicking the magnifying glass at the upper right, between the bar and the first row of images.)
* [The Creative Commons](https://search.creativecommons.org/) itself aggregates a lot of other sites, which you can filter in the search function. Though, for some, going to the source may yield better results.
  * I've found that the [Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page) shows up very reliably in this search.
* Many, many museums and archives have digital repositories available for public use, if you go clicking around, e.g.:
  * [The Getty](https://www.getty.edu/about/whatwedo/opencontent.html)
  * [The National Gallery of Art](https://www.nga.gov/open-access-images.html) (maybe all free?)
  * [The Met](https://www.metmuseum.org/about-the-met/policies-and-documents/image-resources)
  * A LOT of museums in Paris; see [here for a search](https://www.parismuseescollections.paris.fr/en/recherche/avec-image/1) and [here for information](https://ourcommunitynow.com/art/over-100000-artworks-added-to-public-domain-by-paris-museums). NB: You'll have to muddle through with whatever French you've got.
  * [The Wellcome Center](https://wellcomecollection.org/works)
  * AND SO ON. If you have heard of it, and it is a public collection, likely it has an absolute trove of public images.

So, if you have your heart set on the Mona Lisa, you'll have to make do. Same goes for if the thing you crave is in the hands of a private collector. ([Maybe you can trade for an NFT of the same thing?](https://www.bloomberg.com/opinion/articles/2021-04-05/bill-hwang-s-archegos-swap-aftermath-is-awkward-for-banks)) But if you like browsing historical images -- the world is a good place for that! Thanks, Cory; I think?

\-- Tom