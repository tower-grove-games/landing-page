+++
author = "Sam"
categories = ["elizabeth gaskell", "layout", "first edition", "literature", "aesthetics", "design", "hobbyist gaming", "tabletop games", "table games", "board gaming", "board game", "board games"]
date = 2021-08-27T12:00:00Z
description = "You can patch together: years, dissertations, novels, D I S C O U R S E."
lead-image = ""
tags = ["industrialism", "literary history", "literature", "cranford", "elizabeth gaskell", "play", "cards", "aesthetics", "art", "design", "first edition"]
title = "*First Edition* Book Spotlight 3: *Cranford*"

+++
First, my apologies for not writing last week! It was a busy one for me, featuring a (successful, as it turns out!) dissertation defense followed almost immediately by my birthday! So I was much busier _playing_ games last week than I was in writing about them. I got to play a medium-difficulty game of [_Spirit Island_](https://boardgamegeek.com/boardgame/162886/spirit-island), so I was a happy gamer. It’s such a great game that is _so_ complicated, and I love it.

In any event, this week is a new week and I _am_ writing a post today, returning to my series featuring specific book cards for _First Edition_ and the art I’ve chosen for them. Today, I want to focus on [Elizabeth Gaskell](https://en.wikipedia.org/wiki/Elizabeth_Gaskell)’s novel [_Cranford_](https://en.wikipedia.org/wiki/Cranford_(novel)), which was written in 1853. First, a note on Gaskell: while experts in Victorian literature would readily identify her as a major Victorian novelist, she obviously doesn’t enjoy the kind of household name-recognition of [Dickens](https://en.wikipedia.org/wiki/Charles_Dickens), [Charlotte Brontë](https://en.wikipedia.org/wiki/Charlotte_Bront%C3%AB), or [George Eliot](https://en.wikipedia.org/wiki/George_Eliot). Some of that doubtless has to do with gender: many important novelists who were women were long neglected in critical accounts, an imbalance that still marks the discipline even as extensive efforts are being made to correct it. But it also seems like Gaskell’s novels themselves, which generally work less with gothic or thriller tropes than those of the Brontë sisters and which are more limited in scope than some by George Eliot, have led in part to her being relatively overlooked.

Gaskell’s most famous novel is [_North and South_](https://en.wikipedia.org/wiki/North_and_South_(Gaskell_novel)) (1855), which depicts the social upheaval in England that has resulted from the rapid industrialization of the North during the Industrial Revolution. _Cranford_ is also a novel about shifting social conditions and conventions during the nineteenth century, but is set in the fictional small town of Cranford. It is a very episodic novel, almost more of a collection of sketches than a proper novel, all tied together by the place itself.

In looking for a good image for _Cranford_, I wanted something that focused on the streets of a small town rather than on a group of people. After some searching, I hit upon this image:

![](/uploads/cranford.jpg)

I like this picture for several reasons. First, the main building looks like it’s been patched together over the years out of different materials by different owners. It’s so oddly constructed, with weird boxy angles and different shades of brick or stone. I like its contrast with the church that you can see behind it, too. And then, I really like the figure of the woman in front of the house, looking at the empty street. Since she’s looking away from the camera, her presence emphasizes the buildings over the people who may live in them. Even though the picture is from 1886, about 35 years too late, I think it works for this novel.

Let me know what you think!

\-- (Dr.) Sam