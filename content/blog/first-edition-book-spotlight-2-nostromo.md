+++
author = "Sam"
categories = ["literature", "design", "layout", "joseph conrad", "nostromo", "literature", "hobbyist gaming", "table games", "tabletop games", "board games"]
date = 2021-08-13T12:00:00Z
description = "YSK about Joseph Conrad: brilliant, engaging, difficult, definitely more than a little racist."
lead-image = ""
tags = ["nostromo", "joseph conrad", "realism", "art", "texts", "cards", "imperialism", "modernism", "colonialism", "literary history", "literature", "aesthetics", "design"]
title = "*First Edition* Book Spotlight 2: *Nostromo*"

+++
Welcome to the second installment of Sam Talks About Books He Likes! This week, we’re jumping forward quite a bit from Henry Fielding to Joseph Conrad’s [_Nostromo_](https://en.wikipedia.org/wiki/Nostromo), published in 1904. Conrad has long been one of my favorite novelists, and you’ll find that _First Edition_ reflects that, as it contains four of his novels. If you remember one factoid about Conrad, it should be this: English, the language in which he wrote all of his novels, was actually his _third_ language! Conrad is Polish, and upon leaving his country (that is, after being violently cast out of it by the Russians), he joined the French Merchant Marine. After that, he settled in England into his career as a major novelist, writing in English.

Conrad’s favorite novelistic subject was the moral quandaries faced by European men in colonial settings. Needless to say, there are many elements of his novels that haven’t aged particularly well—see, for instance, [Chinua Achebe’s famous essay](https://en.wikipedia.org/wiki/An_Image_of_Africa) about the problematic elements of Conrad’s treatment of native Africans in [_Heart of Darkness_](https://en.wikipedia.org/wiki/Heart_of_Darkness). _Nostromo_ certainly contains some imperialist elements: it centers on a cast of European characters clustered around a silver mine in a fictional South American country, which undergoes several moments of revolution during the novel. Conrad tends to cast the native peoples as extras in their own country, not to mention as caricatures of a range of native stereotypes. As I said, this tendency of Conrad's certainly hasn't aged well, and for a brilliant post-colonial novel that critiques this kind of colonial treatment of South America head-on, I would encourage you to check out [Gabriel García Márquez](https://en.wikipedia.org/wiki/Gabriel_García_Márquez)'s [_One Hundred Years of Solitude_](https://en.wikipedia.org/wiki/One_Hundred_Years_of_Solitude).

While we shouldn’t use _Nostromo_ as a guide for how we think about issues of European imperialism and race—it is more of a primary document that illustrates how Europeans were thinking about themselves and their imperial projects—I _do_ think that it’s one of Conrad’s best novels. To bring all of this together: if Conrad’s favorite topic is thinking about how “civilized” Europeans tend to abandon their morals when they move into “wild,” “uncivilized” spaces, _Nostromo_ is perhaps his best meditation on that topic. That doesn’t excuse its problematic basis, but if there is any merit in Conrad at all—which Achebe would say there is not—_Nostromo_ encapsulates a lot of it.

There’s a lot going on in _Nostromo_: there’s a silver mine, there are several revolutions complete with street skirmishes and mid-speed horseback chases, there’s a lighthouse, a love story, an act of grand larceny. As I try to find good images for the books in _First Edition_, the question I keep asking myself is: what do I remember most when I think about this book? For _Nostromo_, I remember the incredibly tense scene in which the title character, Nostromo, and the French dilletante Martin Decoud attempt to save the latest shipment of silver from the mine from the rampaging revolutionary forces. The pair are attempting a dangerous nautical maneuver in the dark, trying to slip past the revolutionaries’ boats that are clogging the harbor to get to the lighthouse, where they plan to hide the silver. Combing through the database, I came across this picture:

![](/uploads/nostromo_1.jpg)

As you can see, this picture captures a lot of the details of the scene I just described: there are two figures in a boat, with a ship off in the distance. But I don’t think this sketch fits with the mood of the scene: it’s too bright, for one thing, and there doesn’t seem to be a lot of tension or urgency here. So I kept looking.

As I did so, I thought again about what I think the book is most centrally _about_. In the end, it’s right there in the title: _Nostromo_ is a study of the moral choices faced by the title character, the “incorruptible” “capataz de cargadores,” Nostromo. As these epithets indicate, Nostromo is in charge of the dockworkers in the town in which the novel is set, and is generally considered to be extremely trustworthy—a quality that will come into question during the novel. When I stumbled upon this picture, of a dockworker taking a break in early-20th-century Brazil, I knew I had found my picture.

![](/uploads/nostromo_2.jpg)

I love the way that the dockworker is looking at the camera but remains out of focus. I think it perfectly encapsulates how we feel about Nostromo. We can see him, but never totally clearly.

Thanks for reading! Until next week,

\--Sam