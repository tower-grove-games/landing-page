+++
author = "Lauren"
categories = ["recommendations", "appreciation", "overview", "reflection"]
date = 2021-05-07T11:00:00Z
description = "Birds are real!"
lead-image = ""
tags = ["representation", "design", "aesthetics", "change-of-mind", "education", "introduction"]
title = "Learning Bird Appreciation"

+++
Our featherbrained friends, birds, have never been my favorite wildlife creature. Their sharp beaks, wiry claws, and beady, shifty little eyes make me rather uneasy. Then, of course, there are the massive amounts of sticky bird guano (a fancy term for _poop_, which was apparently [a hot trade commodity](https://americanhistory.si.edu/blog/what-load-guano-5-facts-you-didnt-know-about-bird-poop) in the 19th century). If you have ever read the children’s book, [_Don’t Feed the Coos_](https://us.macmillan.com/books/9781250303189), the most accurate portrayal is when the author writes, “Coo poos everywhere. Coo poos covering everything.”

My personal experiences with birds has heavily shaped my feelings toward birds in a not so positive light:

* There was the continuous racket of “cawing” in my backyard by the crows whenever my cat, Tiger, who just wanted to do what cats do, was outside.
* There are the largest, most aggressive seagulls down by the pier at Ivar’s who enjoy many a chip, if not a fried fish. (Yes, I grew up in Seattle, Washington.)
* There was the time that the top of my head was attacked multiple times on two separate occasions by a Red-winged Blackbird at my place of work.

I understand these are all typical instances of bird behavior: alerting other birds of a danger present, the desire for an easy, tasty treat (which we can all appreciate), and protecting one’s territory, especially when it may involve young. Still, reason alone cannot and does not make me feel more warmly toward these feathered creatures.

Cue bird’s saving grace against people like me: portrayals in art. A newfound appreciation and understanding for birds blossomed on my first play of [_Wingspan_](https://boardgamegeek.com/boardgame/266192/wingspan). Thank you, Elizabeth Hargrave, Natalia Rojas, and Ana Maria Martinez! The game’s depictions of the bird on the cards and subtle bird education was the hook I needed. The art is absolutely astounding, with accurate and detailed portrayals of individual birds. Not only is the art an award-winning factor in my eyes, but the game play is intuitive and varied. So a win-win all around in keeping my enthusiasm up.

Though I am not an avid birder, I now walk through nature and possess some understanding of the beauty and tangible value of birds. Birds are important to the way of life in the ecosystem and can be a good indicator of how that ecosystem is thriving. Without their aid, the world would be a rather unpleasant place. So thank you to birds, despite my prior experiences, and thank you to those artists whose creations evoke a sense of understanding and appreciation for our bird populations.

If you’re like me and trying to build further appreciation of birds, here are some children’s books that can help:

[_One Dark Bird_](https://lizgartonscanlon.com/books/one-dark-bird/) by Liz Garton Scanlon

[_How to Find a Bird_](https://www.simonandschuster.com/books/How-to-Find-a-Bird/Jennifer-Ward/9781481467056) by Jennifer Ward

[_Have You Heard the Nesting Bird?_](https://www.barnesandnoble.com/w/have-you-heard-the-nesting-bird-rita-gray/1116226323) by Rita Gray

[_Owl Moon_](https://www.janeyolen.com/owl-moon/) by Jane Yolen

\-- Lauren