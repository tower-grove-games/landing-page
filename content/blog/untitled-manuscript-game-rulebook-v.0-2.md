+++
author = "Tom"
categories = ["exemplar", "rulebooks", "rules", "writing", "revision", "playtesting", "design", "hobbyist gaming", "table games", "tabletop games", "board gaming", "board games", "board game"]
date = 2021-08-10T11:00:00Z
description = "These rules describe a game that you could play if you had the pieces!"
lead-image = ""
tags = ["exemplar", "victory", "endgame", "details", "flow of play", "setup", "components", "objectives", "overview", "framing", "phrasing", "revision", "rulebooks", "rules", "playtesting", "design"]
title = "*Untitled Manuscript Game*: Rulebook v. 0-2"

+++
Hey everyone! Short post today with a long attachment, because I've been working on the rules for _Untitled Manuscript Game_. They aren't pretty -- I am not a layout person! -- but I think they do a decent job describing what goes on when you play the game, anyway. I am welcome to all manner of suggestions for improvement in description moving forward! Post your recs here (using Disqus) or on facebook or twitter or email me at towergrovegames@gmail.com. Thanks for taking a look!

## [THE RULES (as they now stand)](/uploads/umg_rules_0-2.pdf)

\-- Tom