+++
author = "Sam"
categories = ["novels", "books", "art", "graphic design", "revision", "design", "hobbyist games", "tabletop games", "table games", "board games"]
date = 2021-07-30T12:00:00Z
description = "Graphic design schmafic design, let's play games."
lead-image = ""
tags = ["first edition board game", "first edition", "components", "revision", "art", "public domain", "images", "modifications", "playtesting", "layout", "design"]
title = "The Development of *First Edition*’s Book Cards"

+++
This week, I’ve been starting to bear down on the biggest remaining challenge for _First Edition_: finding good, [public domain art](https://towergrovegames.com/blog/copyright-cover-art-and-the-idea-of-property-iv/) for each of the 135 book cards. Lauren has found some really great options for most of the “Rise of the Novel” books, and I have been working this week on getting back in the swing of things, narrowing down the options Lauren has found, and finding new ones on my own.

As we work on getting the cards into their final, glorious shape, I wanted to give you a little glimpse at the various forms these cards have taken over the different stages of _First Edition_’s development.

First is my first prototype, which, as you can see, I made by hand:

![](/uploads/fe_history_1.jpg)

I only played with this prototype a few times, but it definitely served its purpose! It got the game to the table, where it did _not_ play very well but _did_ convince me that there was something fun there. That pushed me to discover NanDeck, which let me make cards digitally using a Mail Merge function. My first version was just a digitized version of the above, but I quickly realized that there needed to be _some_ color. So here’s where that insight led me:

![](/uploads/fe_history_2.jpg)

Next, as the pandemic took hold, I decided that I wanted to put _First Edition_ up on Tabletop Simulator, both for ease of sharing and for playtesting purposes. That let me get a little more creative with the design of the cards, since I didn’t have to print them! Here’s what that looked like:

![](/uploads/fe_history_3.jpg)

Now, as Tom wrote in [his blog post on Alpha Mods](https://towergrovegames.com/blog/first-edition-alpha-mods/), we’ve decided to incorporate a colored band into our card designs to help indicate the general value of any given card. We will keep playing with this (especially to make sure it is colorblind-friendly!), but we are moving forward with trying to incorporate that into the design of the cards. Add some of the art that we’ve found so far, and this is where we are right now:

![](/uploads/fe_history_4.png)

![](/uploads/fe_history_5.png)

This design will likely change, and soon! We are having ongoing meetings and discussion about what we want to accomplish with the graphic design. I still wanted to share these examples, though, just to show you what we’re working with (and to show off how cool the art is looking already!)

More next time,

\--Sam