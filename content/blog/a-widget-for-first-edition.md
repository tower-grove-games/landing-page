+++
author = "Sam"
categories = ["scale", "economies", "writing", "despair", "reflection", "playtesting", "design", "hobbyist games", "tabletop games", "board games"]
date = 2021-07-16T12:00:00Z
description = "First Edition: Silicon Valley Edition, now with widgets, gizmos, futures, and greed!"
lead-image = ""
tags = ["components", "revision", "setup", "auctions", "economics", "strategy", "value", "alpha testing", "playtesting", "design"]
title = "A Widget for *First Edition*?"

+++
OK, so, if Nation A takes 3 hours to produce one Widget and 2 hours to produce one Coconut and Nation B produces a Widget in 2 hours and a Coconut in 1, we would say that Nation B has an _absolute advantage_ in both commodities: it can produce both kinds of goods in less time than it takes Nation A to produce them. However, it would still be beneficial for Nation B to trade Coconuts to Nation A for Widgets, because Nation A has a _comparative advantage_ in Widget production. So, if Nation A spends 8 hours making goods, it can make 2 & 2/3 Widgets or 4 coconuts. In 8 hours, Nation B can make 4 widgets or 8 coconuts. Nation A has a comparative advantage here on Widget production because…

Oh, wait, this is a blog, not my dissertation. Listen, friends: writing a dissertation is hard and unpleasant, but I am learning this week that _finishing_ one is all of that multiplied by like 100.

Nevertheless, when I write to you again this time next week, I shall be shed of this burden! That’s right: my dissertation is due next Wednesday, and boy am I ready to turn it in. Unfortunately _it_ isn’t ready to be turned in, so I must labor on for 6 more days.

Despite this incredibly draining task, we found the time and I found the energy to get a playtest of _First Edition_ in last night! It went well, in the sense that playtests _can_ go well: some of the things we did to try to fix stuff broke other stuff, to varying degrees. But we are pretty sure we know what happened, we learned a lot, we had new ideas, and we are ready to make some adjustments and move forward.

I won’t write too much more today, because I don’t know if you knew this, but I am writing OTHER THINGS right now. I will just touch on one of the problems that came up again with this playtest: one of the players ended one of her turns with essentially no money, leaving her unable to compete in auctions _for a full 4 turns_ (out of 16 total!). Now, this player definitely did a few suboptimal things to help her get into this position, but I think it’s lazy design to just shrug and say, “well, maybe just play my game better?”

That said, we also don’t want to _loosen_ the economy of the game too much. So the challenge is this: how to we introduce a way for players who have overextended themselves to get some quick cash _without_ opening the door for players who _haven’t_ done that to run away with the game?

The solution I want to try is simple: every player gets a token that they can discard _at any time_ (i.e. not just on their turn) to perform either a sell books or a sell to a collector action. There will be no way in the game to get more of these: it’s a one-shot action. If you overextend yourself again, well, I don’t know what to tell you! But also I’m pretty confident that the token will benefit players who have gotten themselves into a sticky situation _considerably more_ than it will benefit players who are just going to get an extra sell action out of it.

We’ll see! This is the joy of playtesting, after all!

Until next time, at which point I will no longer have to be thinking about two of the worst people ever, Ezra Pound and T.S. Eliot!

\--Sam