+++
author = "Sam"
categories = ["fun", "tabletop games", "board games", "conventions", "planning", "excitement", "reflection"]
date = 2021-06-04T12:00:00Z
description = "We can play games places -- but will we?"
lead-image = ""
tags = ["events", "market", "research", "community", "design", "play"]
title = "Getting Back in the Swing of Things"

+++
I know I’ve written about the return of conventions already, but some emails came through this week that have me excited again. First, it looks like the [Stonemaier Design Day](https://store.stonemaiergames.com/products/stonemaier-games-design-day-2021?variant=39368352563281) will be happening in person this year, and Tom and I are hoping to bring a prototype each to that event. The Design Day has been critical to the development of *First Edition*, and I’m excited at the prospect of being able to bring a fresh new prototype to the 2021 Design Day! I’ll let Tom update you on the design he’s working on in a later post, but it’s going to be such a fun experience to have two different games and very different points in their development at the Design Day, with a chance to get some invaluable feedback.

The second email was about [Geekway to the West](https://geekway.com/), and boy was it exciting. Tom and I are making plans to be at Geekway 2021 for at least two of the days, but I’m also thrilled that there will be potentially *4* Geekway conventions in a matter of nine months. Tom and I have been trying to do lots of market research, trying out as many new-to-us games as possible on TTS and from our personal collections (which have of course grown during the pandemic). There’s no better way to do that than at Geekway, which has a great Play-to-Win program featuring lots of recent hotness games *and* an enormous library of older titles that you can check out and try. It’s a great opportunity for doing some *intensive* market research over the course of a few days, and it’s going to be a great time.

I do have to admit that underneath my excitement for all of these upcoming events, I can’t shake some low-level anxiety about being back out and about in larger crowds. The Design Day will be a good first test of this, since the attendance is limited to 110 people (although the space, [*Pieces*](https://www.stlpieces.com/), is of course also smaller than the St. Charles Convention Center). How is it going to feel to be at a convention with thousands of people, though? I guess we’ll just have to see. It’s not even really a matter of legitimate health concerns for me at this point—it’s just that I’ve trained my mind and body (unintentionally, to some degree!) to *freak out* whenever someone comes within 6 feet of me. That kind of learned response is really hard to overcome. I’m looking forward to coming out of my bubble over the second half of this year, though, and these upcoming events seem like a good way to do that. Hey, convention centers are huge! We can always find a hidden-away corner to try out some of these games.

Be kind to each other as we all try to break out of our mental and physical bubbles and get back to normal! It’s going to take time, and that’s OK. I’m looking forward to seeing all of the attendees at the Stonemaier Design Day and Geekway to the West in a few short months!

\--Sam