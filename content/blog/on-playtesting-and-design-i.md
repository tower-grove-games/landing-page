+++
author = "Tom"
categories = ["reflection"]
date = 2021-04-06T11:00:00Z
description = "make games, play games, learn stuff"
lead-image = ""
tags = ["learning", "education", "fun", "balance", "theory", "playtesting", "design"]
title = "On Playtesting and Design (I)"

+++
Sam and I have been having a recurring argument about game design, which I would describe as frustrating the first time, infuriating the second time, and -- like any good running gag -- more and more endearing as our collaboration goes on. (Also like any good gag, it's helpful for understanding what exactly is happening around here, in the big picture.) What it basically comes down to is that Sam is hardworking, conscientious, and decent, while I am lazy, selfish, and obsessed with efficiency. I would rather spend 20 hours crafting a theoretical and roughly numerical system that _suggests_ a game has balance than spend 10 hours playtesting the exact same game to find out what _feels_ balanced.

I'm not going to argue for one approach over the other, in this post. Instead, I want to try to provide a record of where my head is at, at this early stage of developing a decent run of very different games. So I italicized "suggests" and "feels" above, in part to emphasize how neither approach, taken by itself, will guarantee "results" -- in the sense of, a game that both works and is fun to play.

My sense, right now, is that the best games have a little bit of both going on: enough theory/math to get you started and enough playtesting/feel to keep you hooked. I'm inclined to say that games that start with a theme will need the most theory-balancing, while games that start with a mechanic or set of mechanics will need the most practical-balancing.

As an educator, I'm obliged to point out that this is a massive pedagogical problem, no matter what you are trying to convey! For a lot of people, learning grammar is Not Fun, but boy howdy can you have a good time with words once you know what they do! For a lot of people, learning linear algebra is Not Fun, but wow can it let you model a lot of fascinating events!

Games bring the payoff to the fore of learning, which is one reason why they make an effective teaching tool.

Anyway, there's a recent Board Game Design Lab episode with Emerson Matsuuchi about how to use math in games (it's really good, [go listen to it](https://boardgamedesignlab.com/using-math-to-design-better-games-with-emerson-matsuuchi/)), where at one point Emerson says something like:

> "The game was balanced -- but was it fun?"

This question haunts me! I can't remember if he follows up with, "The game was fun -- but was it balanced"? (Cole Wehrle is [on record saying](https://www.dicebreaker.com/companies/wehrlegig-games/interview/cole-wehrle-interview-oath-root-pax-pamir), "we're not trying to make the game fun. We're trying to make the game good," which I find inspiring, terrifying, unreasonable, and true. Also, Cole Wehrle makes fun games.) But it strikes me that, for the best games, it is going to be necessary to ask three questions:

* Is it fun?
* Is it balanced?
* What can we learn from it?

What games answer all three questions spectacularly?