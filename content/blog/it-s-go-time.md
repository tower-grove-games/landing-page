+++
author = "Sam"
categories = ["enthusiasm", "excitement", "hobbyist games", "tabletop games", "board games"]
date = 2021-07-24T11:00:00Z
description = "SAM'S BACK FOR AUCTION"
lead-image = ""
tags = ["auction", "first edition game", "classics", "exploration", "revision", "friendship", "playtesting", "design"]
title = "It's Go Time"

+++
Dear Tower Grove Games fans: I did it! I distributed my dissertation to my committee on Wednesday. It is done, and I am _nearly_ done. There’s still a defense in a month, but in many ways distributing the dissertation is the _real_ finish line.

But Tom told me that I should write about _First Edition_ today, so I will do that now: friends, I am very excited to really crunch into developing this game. We are almost there, and now that Tom and I are both going to be able to devote some time every day—and sometimes the _same_ time, in the same place!—to putting the finishing touches on the mechanics, things are going to come together quickly.

We have some more adjusting to do with employees: testing different combinations to make sure that we have them priced correctly, etc. We want to overhaul the event system, or rather, not the system itself, but the content of the event cards. We want finally to get around to playing more of these older, classic auction games to see if there are any cool auction formats that are transportable into _First Edition_. And then of course we want to finish fine-tuning the art and graphic design so that it looks awesome.

_First Edition_ has been in development with varying degrees of focus and dedication for a little more than three years now. I’m ready to put in that last 20% that Tom mentioned [in his post about Alpha Mods](https://towergrovegames.com/blog/first-edition-alpha-mods/) and get it in your hands. I think you’re going to love it.

\--Sam