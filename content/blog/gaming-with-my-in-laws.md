+++
author = "Sam"
categories = ["group awareness", "complexity", "board games", "strategy", "reflection"]
date = 2021-05-28T12:00:00Z
description = "The best games are the ones you can play."
lead-image = ""
tags = ["attitude", "design", "learning", "groups", "choice", "play"]
title = "Gaming with my In-Laws"

+++
It’s Memorial Day weekend here in the States, and Grace and I are making the short drive to her parents’ house for the first time since last February. We’ve seen her folks throughout the pandemic, but only in a socially-distanced, outdoors kind of way. Now that we’re all safely vaccinated, we’re finally going to be able to stay with them for the weekend.

That’s all cause for celebration, and I intend to keep celebrating these milestones in this venue as they keep happening! But the reason I’m writing about this event here is that every time we go home, the question comes up: which games should we bring?

Grace’s parents are good sports, and they’ve endured a range of hobby games over the years: *[Ticket to Ride](https://boardgamegeek.com/boardgame/9209/ticket-ride)*, *[Munchkin](https://boardgamegeek.com/boardgame/1927/munchkin)* (I know, I know…), *[Villainous](https://boardgamegeek.com/boardgame/256382/disney-villainous)*, *[Pandemic](https://boardgamegeek.com/boardgame/30549/pandemic)*, *[Just One](https://boardgamegeek.com/boardgame/254640/just-one)*, the earliest version of *First Edition*, the list goes on. 

Early in my days in the board game hobby, my mindset was “what’s the heaviest game x group will play with me.” Heaviness was 100% equivalent to “fun” for me at that time, and that mindset led to some *slogs*—I’ll always remember when Grace’s mom got into a sticky situation playing Maleficent in *Villainous* and just got up and started baking something because she couldn’t do anything on her turns.

Grace’s parents are perfectly *capable* of grokking *Villainous*, but it’s not fun for them, and neither are most of the games on that list! You know what they do like? *[Euchre](https://boardgamegeek.com/boardgame/6901/euchre)*. And so do I! I love trick-taking games (maybe not [as much as Tom](https://towergrovegames.com/blog/tom-s-top-5-ish/), but I actually think it’s close! Learning *Bridge* was a formative gaming experience for me, too.) And they are pretty good at *Just One*, too, it turns out.

The point is that as I’ve grown in this hobby, I’ve learned that the important thing is finding a game that everyone in the group will *like*, not a game that everyone will *be able to play*. And, learning to enjoy games at different weights is just good practice as a game designer! It expands your designer toolkit to include mechanics and puzzles that don’t necessarily make it into heavier games.

May your Memorial Day weekend be filled with gameplay sessions that are fun for everyone involved! I know I’m looking forward to some great hands of *Euchre* (and maaaaybe some *[Race For the Galaxy](https://boardgamegeek.com/boardgame/28143/race-galaxy)* on our phones after Grace’s parents have gone to bed!)

--Sam