+++
author = "Tom"
categories = ["not a lawyer", "description", "reflection"]
date = 2021-04-27T12:00:00Z
description = "Tom doesn't know what stealing is"
lead-image = ""
tags = ["copyright", "derivatives", "creative", "legality", "design"]
title = "Copyright, Cover Art, and the Idea of Property (I)"

+++
So Sam has made an excellent game about some kind of old books ([_First Edition_](https://towergrovegames.com/blog/the-state-of-first-edition/)). Not _extremely_ old, I should emphasize -- we're talking about a couple hundred years at most! and I'm developing a game about medieval manuscripts! -- but old enough that I had assumed from the get-go that words like "copyright" and "license" and "rights" would no longer have any meaningful application to our needs. I had assumed that every aspect of the 1719 _Robinson Crusoe_, for example, would be accessible to the great grimy masses of commercialist nerds like us, for creative use, redeployment, interpretation, and -- most importantly -- sale.

I was, as usual, wrong.

Or at least, I think I was wrong, but I would really really like it for a professional lawyer to tell me that I am in fact wrong now, and was still-wrong-but-closer-to-being-right, then. Because **I am not a lawyer**! I just read too many books and like to play games.

Anyway, _Robinson Crusoe_ is probably the most famous early novel, and obviously it is going to appear in _First Edition_, and it's going to be awesome. At time of writing, I am about 95% sure that no one can stop us from using the title _Robinson Crusoe_ and the name of its author, "Daniel Defoe," in Sam's game. Nor can anyone stop us from writing derivative works based on _Rob Cru_, since the terms of the copyright for the text have clearly expired. This makes for [great literature](https://www.abebooks.com/servlet/BookDetailsPL?bi=30764534091&searchurl=kn%3Dconcrete%2Bisland%26sortby%3D17&cm_sp=snippet-_-srp1-_-title1) and [fantastic worldbuilding](https://www.abebooks.com/servlet/BookDetailsPL?bi=30899729189&searchurl=an%3Dcoetzee%26sortby%3D17%26tn%3Dfoe&cm_sp=snippet-_-srp1-_-title1), not to mention [excellent board games](https://boardgamegeek.com/boardgame/121921/robinson-crusoe-adventures-cursed-island)!

I am also about 60% sure that we could use the title and author for a book still within copyright, which isn't very sure at all, but it's more than the 0% I was at in the middle of April. (I am _still_ not a lawyer.) If we slap "_Game of Thrones_ by George R.R. Martin" on a card and sell that card, my impression is that we might be in the clear, technically? Page 2 of the [U.S. Federal Government's _Circular 1: Copyright Basics_](https://www.copyright.gov/circs/circ01.pdf) reads: "Copyright does not protect . . . . Titles, names, short phrases, and slogans."

But I am ALSO 99% convinced that I printed a series of _Game of Thrones_ cards tomorrow for a game about fantasy novels, then sold those cards to you for money, the kind folks at _HBO_ and probably _Bantam Books_ and maybe even [_Fantasy Flight Games_](https://boardgamegeek.com/boardgame/103343/game-thrones-board-game-second-edition) would have unkind words for _Tower Grove Games LLC_; and I am 100% sure that even if they were to be incorrect, those various entities would still have millions and millions of dollars more than _Tower Grove Games LLC_ to spend on lawyers and lawyerly unpleasantries. That would be bad for us! Which marks out a nice line for grown-ups between "what you can do" and "what you should do." (This is something I'm teaching my 2 year old, also, but  except I won't take his playhouse in suit. Probably.)

Bonus caveat! I am also confident _GoT_ is trademarked in every conceivable way a thing can be trademarked, which is its own can of worms that I still understand less than copyright, somehow, maybe mainly because the [US Trademark Office search methods](https://tmsearch.uspto.gov/bin/gate.exe?f=login&p_lang=english&p_d=trmk) are arcane and inscrutable, and not made for humans. Also they are self-declaredly incomplete? The law is dumb, do you know what it says? Does anyone!?

. . . .

Actually, this post is getting long, and I haven't even gotten to the fun part about IMAGES that I meant to get to, so I'll pause here for now and make this a two-or-more-parter ([here](https://towergrovegames.com/blog/copyright-cover-art-and-the-idea-of-property-ii-hundreds-year-old-images-under-copyright/)). In the meantime, if you know about IP/copyright law or know someone who does or know someone who knows someone who does -- let me know via email or twitter or the comments below, because we need all the help we can get!

Once again, I am 100% not a lawyer, not even a little bit, not a jot; and none of this is legal advice.

\-- Tom