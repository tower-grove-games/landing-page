+++
author = "Sam"
categories = ["collaboration", "modification", "playtesting", "revision", "design", "tabletop games", "hobbyist games", "board games"]
date = 2021-07-09T12:00:00Z
description = "Or: how to design games that your father-in-law won't break, on accident!"
lead-image = ""
tags = ["playtesting", "First Edition", "board games", "pricing", "value", "actions", "arrangement", "setup", "components", "revision", "design"]
title = "*First Edition*: Designing and Revising Extra Actions"

+++
It’s [_First Edition_ time](https://towergrovegames.com/blog/the-state-of-first-edition-ii/), [all the time](https://towergrovegames.com/blog/the-state-of-first-edition/), here at Tower Grove Games right now. Well, that and “Sam’s dissertation is due in two weeks” time.

Despite the _intensely_ unpleasant implications of that sentence, we are making strides on _First Edition_, as [Tom’s post earlier this week](https://towergrovegames.com/blog/first-edition-breakdown-card-values/) should indicate. Along with fine-tuning the balance of the book cards themselves, we’ve been hard at work overhauling the employee system.

To measure the distance between where the employees began and where we are now, we need to revisit the original, first draft of _First Edition_. Initially, _First Edition_ had an element of hand management. Players had a hand of action cards, which ranged from, well, actions, such as auctions, etc., to permanent upgrades, like employees. In this model, an “employee” simply gave you an extra action on each of your turns.

This model didn’t last very long: one game in which my father-in-law drew 8 employees and was taking 10 actions per turn to everyone else’s 2 was enough to impress upon me that the value of an action was too great to leave it up to chance whether you could get more of them. And in fact, this play test was also key in my decision to eliminate hands altogether. Instead, I split the original action cards between an event deck and an employee deck. Both are separate from the tableau of books you acquire over the course of the game. In this model, each employee did something different: some let you take a _specific_ action for free on your turn, some gave you discounts on other actions, etc. This is still generally the model we are using for the employees.

![These are probably underpriced.](/uploads/new_employees_1-1.jpg "Sample Employees")

However, as we continued to play test, we realized that the degree of randomness with which these employees came up were going to make them very difficult to price competitively. How do you price an employee who will let you cash in a set—the most valuable action in the game that _everyone_ is always trying to do—as a free action? If it comes up on the first turn, it’s extremely powerful, netting the purchaser 4-5 free, _powerful_ actions over the course of the game. If it comes up in the last turn, it’s much less valuable, and potentially even worthless.

The solution that we (Tom) hit upon is pretty simple: a tiered system of employees. In each game, players will randomly select a certain number of employees from the “early” deck and a certain number from the “late deck,” shuffle each selection separately, and then place the “early” employees on top of the “late” ones. In this way, there might be an incentive to rush through the early employees to get to the better, “late” ones. But doing so would require a big investment, at the moment in the game when players have the least liquidity. Ultimately, we want to make the employees a source of consistently interesting decisions within the game.

I’ll leave it to Tom to talk about how we are going about pricing these employees in this new tiered system. After all, he is the Math Guy!

Until next time

—Sam