+++
author = "Sam"
categories = ["puzzles", "strategy", "design", "hobbyist games", "tabletop games", "board games"]
date = 2021-07-02T12:00:00Z
description = "What is in a game? That which we would call a game by any other name would puzzle."
lead-image = ""
tags = ["opinions", "skill", "forethought", "fun", "puzzles", "strategy", "terms", "design", "theory"]
title = "Puzzles and Their Games"

+++
When I read [Tom’s blog post last week](https://towergrovegames.com/blog/games-and-puzzles-cost-and-price/), my immediate reaction was to disagree. (We’re a fun pair!) After all, games (hobby games, modern games) are _very_ frequently discussed in terms of being puzzles. The fun is in solving the puzzle each time; the variable setup gives you a puzzle to solve each time, etc. We hear phrases like this in podcasts, youtube videos, twitter threads, etc.

As I thought about Tom’s argument, though, I think I may just be disagreeing with the terms he’s using. That is, I think that one thing that Tom is talking about with “puzzles” is games that we might call multi-player solitaire. _Scythe_ is a good example of this kind of game, once everybody is good at it (and at lower player counts). Of course there is interaction, but good players are going to be able to control that interaction, often so that the one or two battles that take place also are going to end the game.

There is tension, sure—_Scythe_ is constantly being compared to a Cold War-style game. But, I think, as people get better at the game, there is actually less of that tension. It is clear what your opponent’s best move is, so they are probably going to do that. The puzzle, as Tom says, is largely solved. At that point, the game becomes more of an exercise, like a Rubik’s Cube: how fast can you solve the puzzle? How many turns ahead can you plan? If your answer is “17,” then you’re probably going to win. So why play?

This is where the expansions come in. As I’ve continued playing _Scythe_, I’ve come to realize that [_The Wind Gambit_](https://stonemaiergames.com/games/scythe/scythe-the-wind-gambit/) and [_The Rise of Fenris_](https://stonemaiergames.com/games/scythe/scythe-the-rise-of-fenris/) expansions are all about introducing more swingy elements into the game, to introduce new elements into the puzzle. The encounters and modular board expansions do this even more explicitly. Moving the game away from the kind of Chess-match feel that develops when all of the players are very good has the effect of turning it away from the multi-player solitaire model of game and into something more interactive.

Now, I probably played 75 games of _Scythe_ before I got to this point. It’s a great game that allows for a lot of plays before you have solved that puzzle. And then those later expansions really do a lot to extend the replayability of the game. And this is where I think I am diverging from Tom’s usage of “puzzle.” I think that I want my games to be puzzles. The problem is _when they are the same puzzle every play_.

So, _Terraforming Mars_ is a puzzle: how do I maximize my resources to yield myself the most points by the end of the game. But the sheer number of cards, corporations, awards and milestones, Colonies, Politics, Preludes—you will never see the same combination of these things twice. Another example is _Gaia Project_: you can play the same combination of factions against each other over and over and yet the puzzle still changes: there are different tech tiles under different tracks, the round bonuses are different, the planets are in a different arrangement, giving you different problems to solve. _Gaia Project_ is definitely a puzzle, it’s just a different puzzle each time. And it’s great. And I’m bad at it!

I think that the ambition behind _Scythe_ was to make a well-balanced, asymmetric game with very little luck. There are encounter cards and factory cards, but other than that the information is mostly open. But ultimately there aren’t enough random elements to keep the game from becoming the same efficiency puzzle each time you play. The genius of _Gaia Project_ is that there are actually _no_ luck elements, but the setup has enough random elements that no two games are the same.

I love both of these games—although I actually love _Scythe_ considerably more. But I think that these differences of approach are really helpful for us as designers: the tension between replayability and _randomness_ drives the interest of so many games, and I think considering these two together helps that come into focus.

Well, now I’m just rambling. I guess I’ll go and play some _Scythe Digital_.

Until next week!

\-- Sam