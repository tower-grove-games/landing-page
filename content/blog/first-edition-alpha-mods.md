+++
author = "Tom"
categories = ["design", "revision", "modification", "value", "breakdown", "explanation", "auction games", "hobbyist games", "tabletop games", "board games"]
date = 2021-07-13T11:00:00Z
description = "Sometimes things are worth more than other things, sometimes less; also, colored pencils!."
lead-image = "/uploads/tiering_2.jpg"
tags = ["color-coding", "tiers", "alpha testing", "first edition", "books", "math", "cost", "price", "value", "revision", "playtesting", "design"]
title = "*First Edition*: Alpha Mods"

+++
It's a little unfair of me to call what I've been working with this week an "Alpha," since Sam's made at least two massive revisions to his high-wire auction game about high-society book collecting since it popped into his head many many moons ago. But I think it's the right term to use for our project, at least in terms of materials. (Besides, the global Actually Make Things Situation is . . . [upsetting](https://twitter.com/TerriblyBland/status/1413606370390257665).) As you can see in the pictures below (and [in prior posts](https://towergrovegames.com/blog/first-edition-designing-and-revising-extra-actions/)), we're talking about thick paper or card stock printed on a home office laser machine, cut with a paper trimmer, and jammed into plastic sleeves. Some book titles are handwritten in Sam's scrawl, others in my scratch. The whole thing is 100% set in terms of theme and 80% set in terms of mechanics -- and yet it remains a work in progress.

That last 20% matters, and it's going to be the difference between a game you enjoy and a game you _love_.

[I wrote last week](https://towergrovegames.com/blog/first-edition-breakdown-card-values/) about how we're measuring and tweaking card values using "an algorithm" which is to say a barely-recursive protocol for measuring the probability of a card being drawn and its expected value (whether used in a set or not) once in play. (Three cheers for the [pickle module](https://docs.python.org/3/library/pickle.html)!) The cards are balanced, which is to say that the interlocking categories used in the game are relatively near to one another in overall expected probablity and return. We didn't even have to change very much to make that happen.

But the cards are not equal in value! That would be boring, first of all, and also not really a game. (It wouldn't even be [a puzzle](https://towergrovegames.com/blog/games-and-puzzles-cost-and-price/)? If all cards were equal in value, you could play with blank paper.) It is crucial to the game that some cards have more value than others. Circumstances being equal, if you choose to buy _The Nunnery for Coquettes_, a card that can earn you at most 20k, instead of _Moll Flanders_, a card that nets you 30k just as the difference between the Auction Price and the Store Price -- well, that's your problem, not mine! 

Of course, you have to have the cash in hand to buy _Moll Flanders_, and its not impossible that that 30k gets eaten up in auction very quickly by your opponents in a bidding war. That's a thing auction games do! They provide a mechanism for determing local value -- i.e. inequal circumstances of acquisition and return.

Still, if you're a first time player, that kind of decision can be overwhelming, to say the least. So after running my "Standard Profit Margin" method, I went ahead and asked PANDAS to sort every card into one of five tiers. Then, I tweaked those tiers a little bit, because my human brain knows things (slower) than my computer's processor (blazing fast).

![](/uploads/satire_cards_ranked.png)

The second tier was supposed to be silver (as in the screenshot above), but it was sort of hard to see IRL, and I like orange, so that's the way it is now on actual paper (as in the screenshot below). Also, I swapped blue and green on the cards, because that's how rainbows are, and my brain was on autopilot. Anyway, this color scheme is by no means final: this is an Alpha Mod. We also have tons to learn about colorblind-friendly approaches here, not to mention basic matters of aesthetics (not my department).

![](/uploads/tiering_1.jpg)

The point being, if you see a Gold card and a Purple card, and you have an open choice to buy either, odds are you want to buy the Gold card, as long as your cashflow permits. But if you see a Green card and a Blue card, and you aren't sure which fits into your strategy, exactly, but you know that your opponent actually might pay at your shop for the Blue card -- you might take the lower tiered card for its situational value! Why not? [Life is just a gamble](https://www.youtube.com/watch?v=wj8aw_B2Q_c), you have to [gamble if you want to win](https://www.youtube.com/watch?v=YsSNX93RGJ0).

![](/uploads/tiering_2.jpg)

The "tiers" are not determining or absolute! But they do give players access to one straightforward mode of assessing value between an intimidating quantity of options. It's a small design decision -- "usually, but not always, this kind of card is worth more than this other kind of card" -- and one made more complicated by end-game bonuses, employee discounts and rebates, and player interaction. It's more common in video games, in my experience. (I've been playing way too many hours of _Slay the Spire_, which 100% does this, but probably the most famous example is _WoW_.) It's also a helpful design decision, we think, to get you started playing _First Edition_.

\-- Tom