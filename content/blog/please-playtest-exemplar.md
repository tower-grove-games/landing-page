+++
author = "tom"
categories = ["play", "mechanics", "design", "art", "files", "playtest", "design", "prototype", "description"]
date = 2023-03-24T02:00:00Z
description = "Exemplar is a game and I want you to play it."
lead-image = "/uploads/everyone_pretending_to_have_fun.JPG"
tags = ["playtest", "book", "medieval", "write", "play", "play and write", "PnP", "prototype", "exemplar"]
title = "Please Playtest Exemplar!"

+++
H E L L O T H E R E

It is me, Tom, for the first time in a long time, and I am extremely pleased to inform you that **the Print-and-Play version of _Exemplar_ has arrived**. I've placed a link to a ZIP file containing all printable components at the bottom of this page.

_Exemplar_ is a game that I have made about scribal compilation and textual arrangement. You can read about its [genesis](https://towergrovegames.com/blog/just-blank-paper/), [first arrangement](https://towergrovegames.com/blog/untitled-manuscript-game-the-words/), [first playtest](https://towergrovegames.com/blog/untitled-manuscript-game-title-overview-first-playtest/), [early design obstacles](https://towergrovegames.com/blog/untitled-manuscript-game-revisiting-exemplars/), [early-ish design obstacles](https://towergrovegames.com/blog/exemplar-moar-playtesting/), and [early graphic design strides](https://towergrovegames.com/blog/exemplar-moar-modifications/) on the blog.

The game has come along in leaps and bounds since I last described it in public. My framework for action economy and scoring has improved, the rules are clearer, and Sam has done some _lovely_ work with his budding talents in graphic design. As you'll see in the photos below, it's _nicer_ now for looking at; and as playtesters will attest, under no duress whatsover, it's playing much, much, much more smoothly than when the idea popped into my head just under 2 years ago, in the flailing days of idea-having and nap-taking that followed my dissertation defense.

So if you are the sort of person who likes **games** or **books**, or **medieval texts**, or **writing stuff down with pencils**, or **games about rectangles**, or **games about decorating books**, or **games about decorating medieval rectangles with pencils in pretend books** -- then wow, listen, you should probably check this thing out.

I would be _thrilled_ if you, dear internet reader, have the time and ability to give this game a go.

![](/uploads/endgame_solo_playtest_1.JPG)

**The game is currently optimized for 3-5 players.** The 2p experience is a rough around the edges (which is pretty normal), and I'm still tinkering with modifications that will improve head-to-head play. Similarly, I've started to look into how designers generally go about creating "solo modes." So, for now, you _can_ play at 2p, but it might be a bit less satisfying, and you cannot play alone (unless you are willing to pretend to be 2 to 5 people). Everything should be working mostly well at 3-5 players, and, frankly, there's not a _lot_ stopping this design from accommodating 6 (or as many as 8, by my best estimate; but things could get zany; and besides, what makes you think I have 8 friends to test that theory on?).

If you do decide to playtest _Exemplar_, please please please do take a moment to fill out [this Google Form](https://forms.gle/EAM6DYBq2DABHgNC8), including a photograph of the final game state for your playtest.

Look at how much fun these humans are pretending to have while playtesting!

![](/uploads/everyone_pretending_to_have_fun.JPG)

Look at this swanky updated Text Card!

![](/uploads/whitebreadmike-3-4.png)

Look at this swanky updated Manuscript Card!

![](/uploads/pearlms-3-4.png)

The game has 8 printable components. The .zip file also has a brief Readme containing instructions for what to do with them.

* Rulebook
* Player mats (1 page, 2 plays per page)
* Text Deck \[8 pages, 60 total cards\]
* Manuscript Deck \[3 pages, 15 total cards\]
* Decoration tiles \[2 pages, 15 total tiles\]
* Player aids \[1 page, 4 aids\]
* Main Market board \[2 pages\]
* Market Board Action Tracker \[2 pages, optional\]
* Manuscript Mini-board \[optional\]

If you want to playtest, you'll also need to provide a few components yourself:

* Pencils (1 per player)
* INK Tokens (10 for each player, distinctly identifiable; we use Wingspan Eggs, but anything smallish will do)
* Manuscript trackers (maximum of 4 for each player, ideally stackable; we use *Wingspan* action cubes, because apparently I guess we haven't played actual *Wingspan* in a bit now, have we?)
* 1st Player Token (ideally something thematic like a quill)

Again, I would be _thrilled times 10 million_ if you have the time and energy to give this a go. Even if you just look at the rules and the components, then tell me about what that was like, I will appreciate it.

I would like to upload a full rules explainer video soon; but I'm also teaching students and prepping the manuscript for my first monograph and raising a child and trying to remember to go outside every now and again and, I am sorry, there are just only so many hours in a day for things like "focus" and "clarity" and "learning how to edit videos." So for now -- there are but files, here. Pray, mess about with them!

And another note, as the landscape surrounding generated content changes literally every day. The art for Text Cards in this _prototype game_ is AI generated, which I fully understand may pose an ethical problem for _commercial enterprise_ and possibly sooner. I hope with my heart of hearts that a publisher picks this game up soon, so that we can work together to find better art from a real artist for the cards. For now, I am sorry to say, I don't have the \~$8000 necessary to pay a good artist a mediocre rate for so much content. And so, as a temporary thing, we are working with the fruits of my collaborator, the literal computer program DALL-E.

The manuscript images are from actual manuscripts, though not always the manuscript listed on the card. Another thing that costs money is copyright permissions from old, fabulous libraries.

Ping me with any rules questions, print freely, and [let me know about your experience through the Google Form](https://forms.gle/EAM6DYBq2DABHgNC8)! (Or, if you prefer, through an email to towergrovegames@gmail.com). Thank you!

\-- Tom

[HERE IS THE LINK TO THE FILE](https://gitlab.com/tower-grove-games/landing-page/-/raw/c6f91ee9436ace39a5c47d2acfe03418bf7ac89e/static/Exemplar--PnP--3_5.zip)

