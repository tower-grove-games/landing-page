+++
author = "Tom"
categories = ["ranking", "nostalgia", "listicle"]
date = 2021-03-26T11:00:00Z
description = "A non-ranked assortment of things I like playing"
lead-image = ""
tags = ["bidding", "cribbage", "dungeon crawlers", "classic euros", "card games"]
title = "Tom's Top 5-ish*"

+++
\*At the present moment, with the stipulation that (a) I adore it when other people make ranked listicles and (b) I absolutely hate making ranked listicles for myself. Everything is a game, games are as good as you make them, some games are easier to make good than others, and here are some games I can't imagine ever not being good for me to play. I'm listing them in the order I type them because you can only take this "list" situation so far, and hierarchies are for Other People:

* **Cribbage** is the first game I learned how to play (\~4 years old), the first game I ever won (\~4 years old plus a couple weeks), and the first game I ever gambled on (\~4 years old plus a couple weeks plus one game -- immediately after winning my first game; and yes, my grandmother took my quarter; but no, I was not skunked; and fine, it was my dad's quarter anyway).
* **Puerto Rico** was -- before even Catan -- my first introduction to the modern board game. We played a five player game, only one person had ever played before, and took nearly four hours. Now at 350+ plays on Boardgamearena, most in the 20 minute range, I'd have to play some games of _Puerto Rico_ (now) while playing a game of _Puerto Rico_ (then).
* **Gloomhaven** has to make the list as the game I've easily spent the most time playing: quarantine tossed my into my third campaign, this one on TTS. Lauren and I played 90+ scenarios between April and July 2018, in the lead-up to the birth of our son, and part of our hospital bag included the necessary materials to run The Money Scenario just in case.
* **Gaia Project** is my go-to heavyweight game. At . . . probably 40+ plays, each extracted with pleas and petitions from my various gaming groups, I'm just astounded that a new perfect information game could feel so endlessly new and unfailingly interesting. Chess, take a seat. (No really, I need like 15 minutes to set up _Gaia Project_; then I'll probably have 5 minutes to lose a blitz match.)
* **Literally any bidding game involving a deck of cards**, I will play now, forever, always. Euchre? I'll go it alone. Spades? You bet your bags. Bridge? 3NT. Oh Shorts? (We're a para-educational website, fam, we use _The Good Place_ expletives) Suits me. Which reminds me, I need to get my own copy of _The Crew_ . . . .

There it is, that's a list, I wrote it using words, and now you can read it. I feel instant pangs of regret -- wherefore art thou, _Terraforming Mars_? not a single game involving dice? do I even _know_ about _Wingspan_, the Game of My House? have I shipped NO COWS TO KANSAS CITY -- but there it is, a list, composed of five items. Dear reader, forgive me my omissions.

\-- Tom