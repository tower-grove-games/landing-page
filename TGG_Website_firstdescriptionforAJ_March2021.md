# Tower Grove Games Website

## Landing Page / Home Page

+ **CONTACT** Box / script for joining our e-mail list.
+ **LOGO** Tower Grove Games in font Lauren chose
  + To be updated once we, like, have a logo.
+ **BIO** We are a board game design startup from St. Louis creating immersive, intuitive, and thought-provoking games for hobbyists and educators.
+ **UPCOMING** Coming soon to Kickstarter:  *Mice N Dice*, a wild and computationally balanced dice competition for who will get the big cheese. Roll, draft, hoard, and score to win!
+ **SOCIAL MEDIA** Links to:
  + *Twitter*: @TowerGroveGames
  + *Insta*: TowerGroveGames
  + *Facebook* [doesn't exist yet]
  + *E-mail*: towergrovegames@gmail.com

## Blog

+ We want to be able to post about once a week, about 300-500 words per post, to a scrollable blog, mostly so we can link to it on Twitter.
+ Would be nice to organize by month and make searchable? But not as high priority at the moment as just having a place to put words.

## [Eventually, pages for individual projects]

+ Mice n Dice
+ First Edition
+ Syntastic!
+ Non Serviam
+ Untitled Manuscript Game
+ What the Dickens
+ Woodland Defenders
